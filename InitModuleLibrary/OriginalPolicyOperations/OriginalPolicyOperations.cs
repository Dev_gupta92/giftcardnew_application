﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Configuration;
using InitModuleLibrary.Model;
using InitModuleLibrary.OriginalPolicyOperations;
using InitModuleLibrary.Data;
using InitModuleLibrary.EmailNotificationHandler;


namespace InitModuleLibrary.OrginalPolicyOperations
{
    public class OriginalPolicyOperations : iOriginalPolicyOperations
    {

        private List<OriginalPolicy> _policies = null;

        public List<OriginalPolicy> allPolicies
        {
            get
            {
                return _policies;
            }

            set
            {
                _policies = value;
            }
        }

        public List<string> getAllAgents() { 
            var allAgents = ConfigurationManager.AppSettings["allAgents"].Split(';').ToList();
            return allAgents;
        }

        public bool getPoliciesFromMirrorTable()
        {
            try
            {
                List<string> allAgents = getAllAgents();
                Console.WriteLine("Aquired Agents: " + allAgents);

                OriginalPolicyFeedData orgDataContext = new OriginalPolicyFeedData();
        
                var listDb = orgDataContext.getPolicies(allAgents);
                if (listDb == null) {
                    return false;
                }
                allPolicies = listDb;
                Console.WriteLine("Policies from Original Table has been copied");
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Original Policy module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;
            }
        }

        public bool insertPoliciesIntoLocalTable()
        {

            try
            {
                if (allPolicies == null || allPolicies.Count == 0)
                {
                    throw new Exception("There was no valid policies to copy from original policy table");
                }
                var orgDataContext = new OriginalPolicyFeedData();
                List<Exception> insertedExceptions = new List<Exception>();
                foreach (var d in allPolicies) {
                    try
                    {
                        orgDataContext.insertCopiedPolicies(d);
                    }
                    catch (Exception ex) {
                        insertedExceptions.Add(ex);
                    }
                  
                }
                if (insertedExceptions.Count == 0) {
                    Console.WriteLine("All policies has been sucessfuly copied from Original table to OriginalPolicyFeed table");
                    return true;
                }
                else
                {
                    Console.WriteLine("There was a problem with inserting policies to local original policies table");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var d in insertedExceptions) {
                        listOfNotInsertedRows += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("Original Policy Feed Exception", "There were "+ allPolicies.Count+" policies.\n"+insertedExceptions.Count+" were not inserted.\nPlease investigate them manually"+ listOfNotInsertedRows);
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Original Policy Feed Exception", ex.Message);
                return false;
            }
          
        }
    }



}

