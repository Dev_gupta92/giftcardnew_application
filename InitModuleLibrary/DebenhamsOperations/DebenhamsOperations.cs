﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Configuration;
using System.IO;
using InitModuleLibrary.Validations;
using InitModuleLibrary.EmailNotificationHandler;
using System.Collections;
using InitModuleLibrary.Model;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using InitModuleLibrary.Data;
using InitModuleLibrary;
using System.Drawing;
using TECIT.TBarCode;
using InitModuleLibrary.DebenhamsOperations;

namespace InitModuleLibrary.DebenhamsOperations
{
    public class DebenhamsOperations : iDebenhamsOperations 
    {
        private MemoryStream _file1Mem = null;
        private MemoryStream _file2Mem = null;
        private MemoryStream _file3Mem = null;
        private MemoryStream _file4Mem = null;
        private List<Model.Barcode> _file1Rows = null;
        private List<Model.Barcode> _file2Rows = null;
        private List<Model.Barcode> _file3Rows = null;
        private List<Model.Barcode> _file4Rows = null;

        private List<barcode> _barcodesFromCurrentMonthST = null;
        private List<barcode> _barcodesFromCurrentMonthAMT = null;
        private string _host = ConfigurationManager.AppSettings["STFPHost"];
        private string _user = ConfigurationManager.AppSettings["STFPUser"];
        private string _pass = ConfigurationManager.AppSettings["STFPPass"];
        private string _debDirectory = ConfigurationManager.AppSettings["DebenhamsDir"];
        public string barcodePrefix = ConfigurationManager.AppSettings["barcodePrefix"];
        private string _pathToImages = ConfigurationManager.AppSettings["pathToImages"];
        private string _absolutePathToImages = ConfigurationManager.AppSettings["absolutePathToImages"];
        private bool _dboffer = Convert.ToBoolean(ConfigurationManager.AppSettings["DBOffer"]);

        public string absolutePathToImages
        {
            get
            {
                return _absolutePathToImages;
            }

            set
            {
                _absolutePathToImages = value;
            }
        }

        public bool DBOffer
        {
            get
            {
                return _dboffer;
            }
            set
            {
                _dboffer = value;
            }
        }
        public string DebebhamsDir
        {
            get
            {
                return _debDirectory;
            }

            set
            {
                _debDirectory = value;
            }
        }
        public  string pathToImages
        {
            get
            {
                return _pathToImages;
            }

            set
            {
                _pathToImages = value;
            }
        }
        
        public string SFTPHost
        {
            get
            {
                return _host;
            }

            set
            {
                _host = value;
            }
        }

        public string SFTPUser
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
            }
        }

        public string SFTPPass
        {
            get
            {
                return _pass;
            }

            set
            {
                _pass = value;
            }
        }

        public MemoryStream File1Mem
        {
            get
            {
                return _file1Mem;
            }

            set
            {
                _file1Mem = value;
            }
        }

        public MemoryStream File2Mem
        {
            get
            {
                return _file2Mem;
            }

            set
            {
                _file2Mem = value;
            }
        }

        public MemoryStream File3Mem
        {
            get
            {
                return _file3Mem;
            }

            set
            {
                _file3Mem = value;
            }
        }

        public MemoryStream File4Mem
        {
            get
            {
                return _file4Mem;
            }

            set
            {
                _file4Mem = value;
            }
        }

        public string[] File1Stringified { get; private set; }
        public string[] File2Stringified { get; private set; }
        public string[] File3Stringified { get; private set; }
        public string[] File4Stringified { get; private set; }


        #region connectAndGrabTheFile
        public bool connectAndGrabTheFile() {
            using (var client = new SftpClient(SFTPHost, SFTPUser, SFTPPass))
            {
                try
                {
                    client.Connect();
                    if (!client.IsConnected)
                    {
                        throw new Exception("Cannot connect to SFTP server");
                    }

                    if (!client.Exists(DebebhamsDir))
                    {
                        throw new Exception("SFTP directory " + DebebhamsDir+ " does not exist");
                    }

                    var myFiles = client.ListDirectory(DebebhamsDir);
                    Validation validator = new Validation();

                    if (validator.debenhamsFilesExists(myFiles, client))
                    {
                        validator.filesStringify();
                        if (!validator.isHeadingValid())
                        {
                            throw new Exception("There was some undefined problem with files' headings");
                        }
                        File1Mem = validator.File1Mem;
                        File2Mem = validator.File2Mem;
                        File1Stringified = validator.File1RowsStringified;
                        File2Stringified = validator.File2RowsStringified;
                        if (validator.File3Mem != null)
                        {
                            File3Mem = validator.File3Mem;
                            File3Stringified = validator.File3RowsStringified;
                        }
                        if (validator.File4Mem != null)
                        {
                            File4Mem = validator.File4Mem;
                            File4Stringified = validator.File4RowsStringified;
                        }
                        Console.WriteLine("Files has been downloaded into the stream");
                    }
                    else
                    {
                        throw new Exception("There are no files in provided directory");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("BarcodeProcessor module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                    return false;
                }
                return true;
            }
        }

        #endregion

        #region ConnectAndGrabTheFileSStep1

        public bool UnprocessedFile()
        {
            using (var client = new SftpClient(SFTPHost, SFTPUser, SFTPPass))
            {
                try
                {
                    client.Connect();
                    if (!client.IsConnected)
                    {
                        throw new Exception("Cannot connect to SFTP server");
                    }

                    if (!client.Exists(DebebhamsDir))
                    {
                        throw new Exception("SFTP directory " + DebebhamsDir + " does not exist");
                    }

                    var myFiles = client.ListDirectory(DebebhamsDir);
                    Validation validator = new Validation();


                    if (validator.unprocesseddebenhamsFilesExists(myFiles, client))
                    {
                        validator.filesStringify();
                        if(!validator.isHeadingValidunProcessedFile())
                        {
                            throw new Exception("There was some undefined problem with files' headings");
                        }
                        File1Mem = validator.File1Mem;
                        File2Mem = validator.File2Mem;

                        File1Stringified = validator.File1RowsStringified;
                        File2Stringified = validator.File2RowsStringified;
                        Console.WriteLine("Files has been downloaded into the stream");
                    }
                    else
                    {
                        throw new Exception("There are no files in provided directory");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("BarcodeProcessor module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                    return false;
                }
                return true;
            }

        }  


        #endregion  

        #region processFiles
        public bool processFiles()
        {

            try {
                if (!checkFilesInTheStream()) throw new Exception("There are no files in memory stream");

                //FILE 1 INSERTING TO DATABASE
                List<Model.Barcode> myBarcodesFile1 = new List<Model.Barcode>();
                List<Exception> file1Exceptions = new List<Exception>();
                foreach (var data in File1Stringified.Skip(1))
                {
                    try
                    {
                        var barcode = new Model.Barcode();
                        var d = data.Split(new char[] { ',' }).ToArray();
                        if (validateFileRow(d))
                        {
                                //BARCODE TABLE: barcodeNumber, barcodeDisplay, pin, value, startDate, endDate 

                                //barcode number 
                                string pattern = @"^\[[0-9]{18}\]$";
                                // Instantiate the regular expression object.
                                var tmp = Regex.Match(d[0], pattern);
                                //if barcode number from csv file does not match the pattern [971234123412341234] skip this line (continue loop)
                                if (!tmp.Success) continue;
                                string barcodeNumber = Regex.Replace(d[0], @"\D+", "").ToString();
                                //after this step barcode number looks like this 971234123412341234

                                barcode.BarcodeNumber = barcodeNumber;

                                barcode.BarcodeDisplay = barcodeNumber.Substring(2);

                                //barcode pin
                                //if barcode pin is longer than 10 chars skip this line (continue loop)
                                if (d[2].Length > 10) continue;
                                barcode.Pin = d[2];

                                //value
                                int barcodeValue = Convert.ToInt16(d[1]);
                                barcode.Value = barcodeValue;

                                //start_date
                                barcode.StartDate = Convert.ToDateTime(d[3]);
                               
                                //end_date
                                barcode.EndDate = Convert.ToDateTime(d[4]);
                               
                                myBarcodesFile1.Add(barcode);
                        
                        }
                    }
                    catch (Exception ex)
                    {
                        file1Exceptions.Add(ex);
                    }
                }
                //FILE 2 INSERTING TO DATABASE
                List<Model.Barcode> myBarcodesFile2 = new List<Model.Barcode>();
                List<Exception> file2Exceptions = new List<Exception>();
                foreach (var data in File2Stringified.Skip(1))
                {
                    try
                    {
                        var barcode = new Model.Barcode();
                        var d = data.Split(new char[] { ',' }).ToArray();
                        if (validateFileRow(d))
                        {

                            //BARCODE TABLE: barcodeNumber, barcodeDisplay, pin, value, startDate, endDate 

                            //barcode number 
                            string pattern = @"^\[[0-9]{18}\]$";
                            // Instantiate the regular expression object.
                            var tmp = Regex.Match(d[0], pattern);
                            //if barcode number from csv file does not match the pattern [971234123412341234] skip this line (continue loop)
                            if (!tmp.Success) continue;
                            string barcodeNumber = Regex.Replace(d[0], @"\D+", "").ToString();
                            //after this step barcode number looks like this 971234123412341234

                            barcode.BarcodeNumber = barcodeNumber;

                            //barcode display
                            //barcode prefix = 97 and can be set in App.config
                            barcode.BarcodeDisplay = barcodeNumber.Substring(2);
                        
                            //barcode pin
                            //if barcode pin is longer than 10 chars skip this line (continue loop)
                            if (d[2].Length > 10) continue;
                            barcode.Pin = d[2];

                            //value
                            int barcodeValue = Convert.ToInt16(d[1]);
                            barcode.Value = barcodeValue;

                            //start_date
                            barcode.StartDate = Convert.ToDateTime(d[3]);
                            //end_date
                            barcode.EndDate = Convert.ToDateTime(d[4]);

                            myBarcodesFile2.Add(barcode);
                        }
                    }
                    catch (Exception ex)
                    {
                        file2Exceptions.Add(ex);
                    }
                }

                //FILE 3 INSERTING TO DATABASE 
                List<Model.Barcode> myBarcodesFile3 = new List<Model.Barcode>();
                List<Exception> file3Exceptions = new List<Exception>();
                if (File3Stringified != null)
                {
                    foreach (var data in File3Stringified.Skip(1))
                    {
                        try
                        {
                            var barcode = new Model.Barcode();
                            var d = data.Split(new char[] { ',' }).ToArray();
                            if (validateFileRow(d))
                            {

                                //BARCODE TABLE: barcodeNumber, barcodeDisplay, pin, value, startDate, endDate 

                                //barcode number 
                                string pattern = @"^\[[0-9]{18}\]$";
                                // Instantiate the regular expression object.
                                var tmp = Regex.Match(d[0], pattern);
                                //if barcode number from csv file does not match the pattern [971234123412341234] skip this line (continue loop)
                                if (!tmp.Success) continue;
                                string barcodeNumber = Regex.Replace(d[0], @"\D+", "").ToString();
                                //after this step barcode number looks like this 971234123412341234

                                barcode.BarcodeNumber = barcodeNumber;

                                //barcode display
                                //barcode prefix = 97 and can be set in App.config
                                barcode.BarcodeDisplay = barcodeNumber.Substring(2);

                                //barcode pin
                                //if barcode pin is longer than 10 chars skip this line (continue loop)
                                if (d[2].Length > 10) continue;
                                barcode.Pin = d[2];

                                //value
                                int barcodeValue = Convert.ToInt16(d[1]);
                                barcode.Value = barcodeValue;

                                //start_date
                                barcode.StartDate = Convert.ToDateTime(d[3]);
                                //end_date
                                barcode.EndDate = Convert.ToDateTime(d[4]);

                                myBarcodesFile3.Add(barcode);
                            }
                        }
                        catch (Exception ex)
                        {
                            file3Exceptions.Add(ex);
                        }
                    }
                }

                //FILE 4 INSERTING TO DATABASE 
                List<Model.Barcode> myBarcodesFile4 = new List<Model.Barcode>();
                List<Exception> file4Exceptions = new List<Exception>();
                if (File4Stringified != null)
                {
                    foreach (var data in File4Stringified.Skip(1))
                    {
                        try
                        {
                            var barcode = new Model.Barcode();
                            var d = data.Split(new char[] { ',' }).ToArray();
                            if (validateFileRow(d))
                            {

                                //BARCODE TABLE: barcodeNumber, barcodeDisplay, pin, value, startDate, endDate 

                                //barcode number 
                                string pattern = @"^\[[0-9]{18}\]$";
                                // Instantiate the regular expression object.
                                var tmp = Regex.Match(d[0], pattern);
                                //if barcode number from csv file does not match the pattern [971234123412341234] skip this line (continue loop)
                                if (!tmp.Success) continue;
                                string barcodeNumber = Regex.Replace(d[0], @"\D+", "").ToString();
                                //after this step barcode number looks like this 971234123412341234

                                barcode.BarcodeNumber = barcodeNumber;

                                //barcode display
                                //barcode prefix = 97 and can be set in App.config
                                barcode.BarcodeDisplay = barcodeNumber.Substring(2);

                                //barcode pin
                                //if barcode pin is longer than 10 chars skip this line (continue loop)
                                if (d[2].Length > 10) continue;
                                barcode.Pin = d[2];

                                //value
                                int barcodeValue = Convert.ToInt16(d[1]);
                                barcode.Value = barcodeValue;

                                //start_date
                                barcode.StartDate = Convert.ToDateTime(d[3]);
                                //end_date
                                barcode.EndDate = Convert.ToDateTime(d[4]);

                                myBarcodesFile4.Add(barcode);
                            }
                        }
                        catch (Exception ex)
                        {
                            file4Exceptions.Add(ex);
                        }
                    }
                }

                //VALIDATION
                if (myBarcodesFile1.Count == 0 && myBarcodesFile2.Count == 0) throw new Exception("Both csv files do not contain valid data");
                if (myBarcodesFile1.Count == 0) throw new Exception("File 1 does not contains valid data");
                if (myBarcodesFile2.Count == 0) throw new Exception("File 2 does not contains valid data");
                if (myBarcodesFile3 != null && myBarcodesFile3.Count != 0)
                {
                    if (myBarcodesFile3.Count == 0) throw new Exception("File 3 does not contains valid data");
                }
                if (myBarcodesFile4 != null && myBarcodesFile4.Count != 0)
                {
                    if (myBarcodesFile4.Count == 0) throw new Exception("File 4 does not contains valid data");
                }
                if (file1Exceptions.Count > 0 && file2Exceptions.Count > 0) {
                    string additionalMessage = "\nThere were following exceptions: \n";
                    foreach (var d in file1Exceptions) {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    foreach (var e in file2Exceptions)
                    {
                        additionalMessage += "\n Exception Message " + e.Message + ", Inner Exception: " + e.InnerException + ", Exception Source: " + e.Source + ", Exception StackTrace: " + e.StackTrace + "\n";
                    }
                    throw new Exception("Some of the barcodes data are wrong. \n"+additionalMessage);
                }
                if (file1Exceptions.Count > 0)
                {
                    string additionalMessage = "\nThere were following exceptions: \n";
                    foreach (var d in file1Exceptions)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    throw new Exception("Some of the barcodes data in file 1 are wrong. \n" + additionalMessage);
                }
                if (file2Exceptions.Count > 0)
                {
                    string additionalMessage = "\nThere were following exceptions: \n";
                   foreach (var e in file2Exceptions)
                    {
                        additionalMessage += "\n Exception Message " + e.Message + ", Inner Exception: " + e.InnerException + ", Exception Source: " + e.Source + ", Exception StackTrace: " + e.StackTrace + "\n";
                    }
                    throw new Exception("Some of the barcodes data in file 2 are wrong. \n" + additionalMessage);
                }
                if (file3Exceptions.Count > 0)
                {
                    string additionalMessage = "\nThere were following exceptions: \n";
                    foreach (var f in file3Exceptions)
                    {
                        additionalMessage += "\n Exception Message " + f.Message + ", Inner Exception: " + f.InnerException + ", Exception Source: " + f.Source + ", Exception StackTrace: " + f.StackTrace + "\n";
                    }
                    throw new Exception("Some of the barcodes data in file 3 are wrong. \n" + additionalMessage);
                }
                if (file4Exceptions.Count > 0)
                {
                    string additionalMessage = "\nThere were following exceptions: \n";
                    foreach (var f in file4Exceptions)
                    {
                        additionalMessage += "\n Exception Message " + f.Message + ", Inner Exception: " + f.InnerException + ", Exception Source: " + f.Source + ", Exception StackTrace: " + f.StackTrace + "\n";
                    }
                    throw new Exception("Some of the barcodes data in file 4 are wrong. \n" + additionalMessage);
                }

                //ASSIGNING VALUES TO PRIVATE PROPERTIES
                _file1Rows = myBarcodesFile1;
                _file2Rows = myBarcodesFile2;
                if (myBarcodesFile3 != null && myBarcodesFile3.Count != 0)
                    _file3Rows = myBarcodesFile3;
                if (myBarcodesFile4 != null && myBarcodesFile4.Count != 0)
                    _file4Rows = myBarcodesFile4;
                Console.WriteLine("Files has been processed sucessfully");
            }
            catch (Exception ex) {
                Console.WriteLine("There were some errors while processing files");
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Barcode Processor Exception", ex.Message);
                return false;
            }
            
            return true;

        }
        #endregion

        #region validateFileRow
        public bool validateFileRow(string[] d)
        {
            try
            {
                for (int i = 0; i < d.Length; i++)
                {
                    if (String.IsNullOrWhiteSpace(d[i]))
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex) {
                return false;
            }
            return true;
        }
        #endregion

        #region checkFilesInTheStream
        public bool checkFilesInTheStream()
        {
            if (File1Mem != null && File2Mem != null)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region insertFilesToDatabase
        public bool insertFilesToDatabase()
        {
            try
            {
                //FILE 1
                var myBarcodesFile1 = _file1Rows;
                List<Exception> insertExceptionFile1 = new List<Exception>();
                foreach (var barcodeRow in myBarcodesFile1)
                {
                    try
                    {
                        var bc = new BarcodeData();
                        bc.insertBarcode(barcodeRow);
                    }
                    catch (Exception ex)
                    {
                        insertExceptionFile1.Add(ex);
                    }
                }

                //FILE 2
                var myBarcodesFile2 = _file2Rows;
                List<Exception> insertExceptionFile2 = new List<Exception>();
                foreach (var barcodeRow in myBarcodesFile2)
                {
                    try
                    {
                        var bc = new BarcodeData();
                        bc.insertBarcode(barcodeRow);
                    }
                    catch (Exception ex)
                    {
                        insertExceptionFile2.Add(ex);
                    }
                }

                //FILE 3
                List<Exception> insertExceptionFile3 = new List<Exception>();
                if (_file3Rows != null)
                {
                    var myBarcodesFile3 = _file3Rows;
                    foreach (var barcodeRow in myBarcodesFile3)
                    {
                        try
                        {
                            var bc = new BarcodeData();
                            bc.insertBarcode(barcodeRow);
                        }
                        catch (Exception ex)
                        {
                            insertExceptionFile3.Add(ex);
                        }
                    }
                }

                //FILE 4
                List<Exception> insertExceptionFile4 = new List<Exception>();
                if (_file4Rows != null)
                {
                    var myBarcodesFile4 = _file4Rows;
                    foreach (var barcodeRow in myBarcodesFile4)
                    {
                        try
                        {
                            var bc = new BarcodeData();
                            bc.insertBarcode(barcodeRow);
                        }
                        catch (Exception ex)
                        {
                            insertExceptionFile4.Add(ex);
                        }
                    }
                }

                if (insertExceptionFile1.Count > 0 && insertExceptionFile2.Count > 0)
                {
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var d in insertExceptionFile1) {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace+"\n";
                    }
                    foreach (var e in insertExceptionFile2)
                    {
                        additionalMessage += "\n Exception Message " + e.Message + ", Inner Exception: " + e.InnerException + ", Exception Source: " + e.Source + ", Exception StackTrace: " + e.StackTrace + "\n";
                    }
                    throw new Exception("There was a problem with inserting the rows to Database \nfrom file1 and file2 \nFile 1: " + insertExceptionFile1.Count + " were not inserted \nFile2: " + insertExceptionFile2.Count + " were not inserted\n"+ additionalMessage);
                  }

                if (insertExceptionFile1.Count > 0) {
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var d in insertExceptionFile1)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    throw new Exception("There was a problem with inserting the rows to Database \nfrom file1 \n " + insertExceptionFile1.Count + " were not inserted\n"+ additionalMessage);
                }
                if (insertExceptionFile2.Count > 0) {
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var e in insertExceptionFile2)
                    {
                        additionalMessage += "\n Exception Message " + e.Message + ", Inner Exception: " + e.InnerException + ", Exception Source: " + e.Source + ", Exception StackTrace: " + e.StackTrace + "\n";
                    }
                    throw new Exception("There was a problem with inserting the rows to Database \nfrom file2 \n " + insertExceptionFile2.Count + " were not inserted\n"+ additionalMessage);
                }
                if (insertExceptionFile3.Count > 0 && DBOffer == true)
                {
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var f in insertExceptionFile3)
                    {
                        additionalMessage += "\n Exception Message " + f.Message + ", Inner Exception: " + f.InnerException + ", Exception Source: " + f.Source + ", Exception StackTrace: " + f.StackTrace + "\n";
                    }
                    throw new Exception("There was a problem with inserting the rows to Database \nfrom file3 \n " + insertExceptionFile3.Count + " were not inserted\n" + additionalMessage);
                }

                if (insertExceptionFile4.Count > 0 && DBOffer == true)
                {
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var f in insertExceptionFile4)
                    {
                        additionalMessage += "\n Exception Message " + f.Message + ", Inner Exception: " + f.InnerException + ", Exception Source: " + f.Source + ", Exception StackTrace: " + f.StackTrace + "\n";
                    }
                    throw new Exception("There was a problem with inserting the rows to Database \nfrom file4 \n " + insertExceptionFile4.Count + " were not inserted\n" + additionalMessage);
                }
                Console.WriteLine("Data from files have been successfully inserted to database");
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine("There were some errors while inserting files");
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Barcode Processor Exception", ex.Message);
                return false;
            }
        }
        #endregion

        #region hasThisMonthBeenProcessed
        public bool hasThisMonthBeenProcessed()
        {
            DateTime nowDate = DateTime.Now;
            string currentYear = nowDate.Year.ToString();
            string currentMonth = nowDate.ToString("MM");
            var mt = new MonthTrackerData();
            if (mt.getMonthTrackerbyMonthAndYear(currentMonth,currentYear) != null) {
                return true;
            }

            return false;
        }
        #endregion

        #region setHasBeenInsertedToTrue
        public bool setHasBeenInsertedToTrue()
        {
            DateTime nowDate = DateTime.Now;
            string currentYear = nowDate.Year.ToString();
            string currentMonth = nowDate.ToString("MM");
            var mt = new MonthTrackerData();
            if (mt.setHasBeenInsertedToTrue(currentMonth, currentYear) == true)
            {
                Console.WriteLine("The flag HasBeenInserted has been set to true succesfully");
                return true;
            }
            Console.WriteLine("There was a problem with updating HasBeenInserted flag to true for current month");
            EmailHandler emailHandler = new EmailHandler();
            emailHandler.sendEmailWithExceptionOrError("BarcodeProcessor module exception", "There was a problem with updating HasBeenInserted flag to true for current month");
            return false;
          
        }

        #endregion

        #region setHasBeenUpdatedToTrue
        public bool setHasBeenUpdatedToTrue()
        {
            DateTime nowDate = DateTime.Now;
            string currentYear = nowDate.Year.ToString();
            string currentMonth = nowDate.ToString("MM");
            var mt = new MonthTrackerData();
            if (mt.setImagesCreatedToTrue(currentMonth, currentYear) == true)
            {
                Console.WriteLine("The flag HasBeenUpdated has been set to true succesfully");
                return true;
            }
            Console.WriteLine("There was a problem with updating HasBeenUpdated flag to true for current month");
            return false;
        }

        #endregion

        #region updateBarcodeRow
        public bool updateBarcodeRow(barcode barcodeRow, int tripType)
        {
            try {
                DateTime nowDate = DateTime.Now;
                string currentYear = nowDate.Year.ToString();
                string currentMonth = nowDate.ToString("MM");
                string subfolder = String.Empty;
                #region old Code
                //int value = 5;
                //if (tripType == 1)
                //{
                //    subfolder = "ST/";
                //    value = 5;
                //}
                //else if (tripType == 2)
                //{
                //    subfolder = "AMT/";
                //    value = 10;
                //}
                #endregion old Code
                int value = barcodeRow.Value;
                if (value == 5)
                {
                    subfolder = "fivepound/";
                }
                else if (value == 10)
                {
                    subfolder = "tenpound/";
                }
                else if (value == 15)
                {
                    subfolder = "fifteenpound/";
                }
                else if (value == 20)
                {
                    subfolder = "twentypound/";
                }
                string imagePath = currentYear + "_" + currentMonth + "/" + subfolder;
                string barcodeNumber = barcodeRow.BarcodeNumber;
                string pathToBeInsertedToDb = imagePath + barcodeNumber + ".jpg";
                barcodeRow.ImageDirectory = pathToBeInsertedToDb;
                var bc = new BarcodeData();
                if (bc.updateBarcode(barcodeRow, value, nowDate.Year, nowDate.Month)) {
                    return true;
                }
                return false;
            } catch (Exception ex) {
                throw ex;
            }

        }
        #endregion

        #region getBarcodes
        public bool getBarcodes()
        {
            try
            {
                var bc = new BarcodeData();
                DateTime nowDate = DateTime.Now;
                int currentYear = nowDate.Year;
                int currentMonth = nowDate.Month;
                
                var barcodeListST = bc.getBarcodes(currentMonth, currentYear , 1);
                var barcodeListAMT = bc.getBarcodes(currentMonth, currentYear, 2);

                if (barcodeListST.Count == 0 && barcodeListAMT.Count == 0)
                {
                    throw new Exception("There are no barcodes data from current month (ST and AMT) to read");
                }

                if (barcodeListST.Count == 0)
                {
                    throw new Exception("There are no barcodes data from current month (ST) to read");
                }
                if (barcodeListAMT.Count == 0)
                {
                    throw new Exception("There are no barcodes data from current month (AMT) to read");
                }
                Console.WriteLine("Barcodes numbers for ST and AMT have been successfully retrieved");
                _barcodesFromCurrentMonthST = barcodeListST;
                _barcodesFromCurrentMonthAMT = barcodeListAMT;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageProcessor module exception", ex.Message);
                return false;
            }
           
        }
        #endregion

        #region createBarcodesImagesAndUpdateDatabase
        public bool createBarcodesImagesAndUpdateDatabase()
        {
            try
            {
                List<Exception> createAndUpdateExceptionsList1 = new List<Exception>();
                foreach (var barcodeRow in _barcodesFromCurrentMonthST)
                {
                    try
                    {
                        //single trip
                        if (createBarcodeImage(barcodeRow, 1))
                        {
                            //image has been created
                            updateBarcodeRow(barcodeRow, 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        createAndUpdateExceptionsList1.Add(ex);
                    }
                }
                List<Exception> createAndUpdateExceptionsList2 = new List<Exception>();
                foreach (var barcodeRow in _barcodesFromCurrentMonthAMT)
                {
                    try
                    {
                        //annual multi trip
                        if (createBarcodeImage(barcodeRow, 2))
                        {
                            //image has been created
                            updateBarcodeRow(barcodeRow, 2);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message+"\n");
                        createAndUpdateExceptionsList2.Add(ex);
                    }
                }
                if (createAndUpdateExceptionsList1.Count > 0 && createAndUpdateExceptionsList2.Count > 0)
                {
                    EmailHandler emailHandler = new EmailHandler();
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var d in createAndUpdateExceptionsList1)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    foreach (var e in createAndUpdateExceptionsList2)
                    {
                        additionalMessage += "\n Exception Message " + e.Message + ", Inner Exception: " + e.InnerException + ", Exception Source: " + e.Source + ", Exception StackTrace: " + e.StackTrace + "\n";
                    }
                    string message = "There was a problem with creating barcodes images or updating images directory for ST and AMT barcodes for current month \nSingle Trip: " + createAndUpdateExceptionsList1.Count + " images/rows were not created/updated \nAnnual Multi Trip: " + createAndUpdateExceptionsList2.Count + " images/rows were not created/updated \n " + additionalMessage;
                    throw new Exception(message);
                }
                else if (createAndUpdateExceptionsList1.Count > 0)
                {
                    EmailHandler emailHandler = new EmailHandler();
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var d in createAndUpdateExceptionsList1)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    string message = "There was a problem with creating barcodes images or updating images directory for ST barcodes for current month \nSingle Trip: " + createAndUpdateExceptionsList1.Count + " images/rows were not created/updated \n" + additionalMessage;
                    throw new Exception(message);
                }
                else if (createAndUpdateExceptionsList2.Count > 0)
                {
                    EmailHandler emailHandler = new EmailHandler();
                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var d in createAndUpdateExceptionsList2)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    string message = "There was a problem with creating barcodes images or updating images directory for AMT barcodes for current month \nAnnual Multi Trip: " + createAndUpdateExceptionsList2.Count + " images/rows were not created/updated \n" + additionalMessage;
                    throw new Exception(message);
                }
                Console.WriteLine("Images have been generated and paths to them have been updated in database");
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine("There were some errors while generating images and updating the barcodes");
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageGenerator module Exception", ex.Message);
                return false;
            }
           
        }
        #endregion

        #region createBarcodeImage
        public bool createBarcodeImage(barcode barcodeRow, int tripType)
        {
            if (barcodeRow.BarcodeNumber == null) {
                return false;   
            }
            try
            {
                //specify barcode data
                string barcodeNumber = barcodeRow.BarcodeNumber;
                if ((string.IsNullOrEmpty(getPathToBarcodeImagesFolder(tripType, barcodeRow.Value)) == true)) {
                    return false;
                }
                string imagePath = getPathToBarcodeImagesFolder(tripType,barcodeRow.Value);
                //instantiate barcode studio class
                TECIT.TBarCode.Barcode barcode = new TECIT.TBarCode.Barcode();
                barcode.License("Rock Insurance Services Ltd GB-RH10 1DQ", LicenseType.Site, 1, "50F22D07CFA51DFAFD6D355CAF170443", TBarCodeProduct.Barcode2D);
               
                barcode.Data = barcodeNumber; //18 digits code
                barcode.BarcodeType = BarcodeType.EanUcc128; //barcode format
                barcode.ActiveTextIndex = 0;
                barcode.IsTextVisible = false; //show barcode number under the barcodes lines
                barcode.Dpi = 300; //specify DPI for barcode
                barcode.BoundingRectangle = new Rectangle(0, 0, 435, 124); //generate barcode 435px width x 124px height
                string fullPath = imagePath + barcodeNumber;
                // save the barcode as image
                barcode.Draw(fullPath, ImageType.Jpg);
                return true;
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        #endregion

        #region getPathToBarcodeImagesFolder
        public string getPathToBarcodeImagesFolder(int tripType, int value) {
            //specify a folder name 
            DateTime nowDate = DateTime.Now;
            string currentYear = nowDate.Year.ToString();
            string currentMonth = nowDate.ToString("MM");
            string subfolder = String.Empty;
            #region old Code
            //if (tripType == 1)
            //{
            //    subfolder = "ST/";
            //}
            //else if (tripType == 2)
            //{
            //    subfolder = "AMT/";
            //}
            //else
            //{
            //    return "";
            //}
            #endregion  old Code

            if (value == 5)
            {
                subfolder = "fivepound/";
            }
            else if (value == 10)
            {
                subfolder = "tenpound/";
            }
            else if (value == 15)
            {
                subfolder = "fifteenpound/";
            }
            else if (value == 20)
            {
                subfolder = "twentypound/";
            }
            else
            {
                return "";
            }

            string fullPath = absolutePathToImages +"/"+ currentYear + "_" + currentMonth + "/" + subfolder;
            return fullPath;
        }
        #endregion

        #region createFolderStructureForCurrentMonth
        public bool createFolderStructureForCurrentMonth()
        {
            string imagesPath = absolutePathToImages;
            if (!Directory.Exists(imagesPath)) {
                return false;
            }
            try
            {
                DateTime nowDate = DateTime.Now;
                string currentYear = nowDate.Year.ToString();
                string currentMonth = nowDate.ToString("MM");
                string subfolder = "/"+currentYear + "_" + currentMonth + "/";
                //creating year_month folder
                if (!Directory.Exists(imagesPath + subfolder))
                {
                    Directory.CreateDirectory(imagesPath + subfolder);
                }

                ////ST subfolder
                //if (!Directory.Exists(imagesPath + subfolder + "ST/"))
                //{
                //    Directory.CreateDirectory(imagesPath + subfolder + "ST/");
                //}
                //AMT subfolder
                //if (!Directory.Exists(imagesPath + subfolder + "AMT/"))
                //{
                //    Directory.CreateDirectory(imagesPath + subfolder + "AMT/");
                //}

                if (!Directory.Exists(imagesPath + subfolder + "fifteenpound/"))
                {
                    Directory.CreateDirectory(imagesPath + subfolder + "fifteenpound/");
                }
                if (!Directory.Exists(imagesPath + subfolder + "fivepound/"))
                {
                    Directory.CreateDirectory(imagesPath + subfolder + "fivepound/");
                }
                if (!Directory.Exists(imagesPath + subfolder + "tenpound/"))
                {
                    Directory.CreateDirectory(imagesPath + subfolder + "tenpound/");
                }
                if (!Directory.Exists(imagesPath + subfolder + "twentypound/"))
                {
                    Directory.CreateDirectory(imagesPath + subfolder + "twentypound/");
                }

                Console.WriteLine("Directory structure has been successfully created");
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine("There were some errors while processing files");
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Barcode Processor Exception", ex.Message);
                return false;
            }
        }
        #endregion

        #region checkWhetherTheImageDirectoryForCurrentMonthExists
        public bool checkWhetherTheImageDirectoryForCurrentMonthExists() {
            string imagesPath = absolutePathToImages;
            if (!Directory.Exists(imagesPath))
            {
                return false;
            }
            try
            {
                DateTime nowDate = DateTime.Now;
                string currentYear = nowDate.Year.ToString();
                string currentMonth = nowDate.ToString("MM");
                string subfolder = "/"+currentYear + "_" + currentMonth + "/";
                //creating year_month folder
                if (!Directory.Exists(imagesPath + subfolder))
                {
                    return false;
                }

                #region old Code
                ////ST subfolder
                //if (!Directory.Exists(imagesPath + subfolder + "ST/"))
                //{
                //    return false;
                //}
                ////AMT subfolder
                //if (!Directory.Exists(imagesPath + subfolder + "AMT/"))
                //{
                //    return false;
                //}
                #endregion old Code

                //5Pound subfolder
                if (!Directory.Exists(imagesPath + subfolder + "fivepound/"))
                {
                    return false;
                }
                //10Pound subfolder
                if (!Directory.Exists(imagesPath + subfolder + "tenpound/"))
                {
                    return false;
                }
                //15Pound subfolder
                if (!Directory.Exists(imagesPath + subfolder + "fifteenpound/") && DBOffer == true)
                {
                    return false;
                }

                //20Pound subfolder
                if (!Directory.Exists(imagesPath + subfolder + "twentypound/") && DBOffer == true)
                {
                    return false;
                }

                Console.WriteLine("Directory structure exists");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("There were some errors while processing files");
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageGenarator module error", ex.Message);
                return false;
            }
        }
        #endregion

        #region hasThisMonthImagesPathsBeenAllocated
        public bool hasThisMonthImagesPathsBeenAllocated()
        {
            DateTime nowDate = DateTime.Now;
            string currentYear = nowDate.Year.ToString();
            string currentMonth = nowDate.ToString("MM");
            var mt = new MonthTrackerData();
            if (mt.checkIfThisMonthImagesPathsBeenAllocated(currentMonth, currentYear) == true)
            {
                string message = "Images paths have been already allocated";
                Console.WriteLine(message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageGenerator Exception", message);
                return true;
            }
            Console.WriteLine("Images paths have not been updated yet");
            return false;
        }
        #endregion

        #region setThisMonthImagesPathsAllocatedToTrue
        public bool setThisMonthImagesPathsAllocatedToTrue()
        {
            DateTime nowDate = DateTime.Now;
            string currentYear = nowDate.Year.ToString();
            string currentMonth = nowDate.ToString("MM");
            var mt = new MonthTrackerData();
            if (mt.setThisMonthImagesPathsAllocatedToTrue(currentMonth, currentYear) == true)
            {
                Console.WriteLine("Flag ThisMonthImagesPathsAllocatedToTrue has been set to true");
                return true;
            }
            Console.WriteLine("There was a problem with setting ThisMonthImagesPathsAllocatedToTrue flaf to true");
            return false;
        }
        #endregion

    }



}

