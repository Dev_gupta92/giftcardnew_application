﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.IO;
using InitModuleLibrary.Model;
using InitModuleLibrary.Data;

namespace InitModuleLibrary.DebenhamsOperations 
{
    interface iDebenhamsOperations
    {
        
        string SFTPHost     { get; set; }
        string SFTPUser     { get; set; }
        string SFTPPass     { get; set; }
        string DebebhamsDir { get; set; }
        MemoryStream File1Mem { get; set; }
        MemoryStream File2Mem { get; set; }

        bool processFiles();
        bool insertFilesToDatabase();
        bool connectAndGrabTheFile();
        bool checkFilesInTheStream();
        bool validateFileRow(string[] d);
        bool hasThisMonthBeenProcessed();
        bool hasThisMonthImagesPathsBeenAllocated();
        bool setThisMonthImagesPathsAllocatedToTrue();
        bool setHasBeenInsertedToTrue();
        bool setHasBeenUpdatedToTrue();
        bool getBarcodes();
        bool createBarcodesImagesAndUpdateDatabase();
        bool createFolderStructureForCurrentMonth();
        bool createBarcodeImage(barcode barcodeRow, int tripType);
        bool updateBarcodeRow(barcode barcodeRow,int tripType);
    }
}
