﻿using InitModuleLibrary.EmailNotificationHandler;
using InitModuleLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;


namespace InitModuleLibrary.Data
{
    public class MasterData
    {
        public MasterData() { }

        OriginalPolicyDataContext myContext = new OriginalPolicyDataContext();

        GiftcardAppDataContext myLocalContext = new GiftcardAppDataContext();

        public List<PurePrintModel> getPurePointDataFromOriginalTable()
        {
            try
            {
                PurePrintDataContext myPurePrintContext = new PurePrintDataContext();

                DateTime yesterday = DateTime.Today.AddDays(-1);
                DateTime fromTime = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 0, 0, 0, 0);
                DateTime toTime = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 23, 59, 59, 999);
                var myList = (from d in myPurePrintContext.PurePrints
                              where d.AgentName.Contains("debenhams") && d.CreatedDate >= fromTime && d.CreatedDate <= toTime
                              select new PurePrintModel
                              {
                                  CertificateReference = d.PolicyNumber,
                                  AgentId = d.AgentId,
                                  Status = d.Type,
                                  Email = d.Email,
                                  AgentName = d.AgentName,
                                  MessageName = d.MessageName,
                                  Title = d.Title,
                                  FirstName = d.FirstName,
                                  LastName = d.LastName,
                                  DateCreated = Convert.ToDateTime(d.CreatedDate),
                                  MasterPolicyUpdated = false,
                                  MasterPolicyUpdatedDate = null
                              }).ToList();
                if (myList.Count == 0) throw new Exception("There were no data in original PurePoint table (bounce, softbounce etc. emails table) to be copied to PurePointFeed (this might not be an error - investigate it manually)");

                return myList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", ex.Message);
                return null;
            }

        }

        public List<RefusalData> getAllDataForRefusalsEmails()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var policyMasterData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.LastStatus == 13 && a.HasValidEmail == true && a.HasTheRefusalEmailBeenSent != true).ToList();
                    List<RefusalData> refusalData = new List<RefusalData>();


                    if (policyMasterData.Count > 0)
                    {
                        foreach (var d in policyMasterData)
                        {
                            //instantiate the RefusalData object
                            RefusalData newRecord = new RefusalData();
                            newRecord.CertificateReference = d.CertificateReference;
                            newRecord.TripTypeId = Convert.ToInt16(d.TripTypeId);
                            newRecord.DateInserted = DateTime.Now;
                            newRecord.LastModified = DateTime.Now;
                            newRecord.IsCancelled = d.IsCancelled;
                            newRecord.IsEligible = d.IsEligible;
                            newRecord.IsPaid = d.IsPaid;
                            newRecord.PromotionalCode = d.PromotionalCode;
                            newRecord.Title = d.Title;
                            newRecord.FirstName = d.FirstName;
                            newRecord.LastName = d.LastName;
                            newRecord.DateOfBirth = d.DateOfBirth;
                            newRecord.Age = d.Age;
                            newRecord.AddressLineOne = d.AddressLineOne;
                            newRecord.AddressLineTwo = d.AddressLineTwo;
                            newRecord.AddressLineThree = d.AddressLineThree;
                            newRecord.CountyState = d.CountyState;
                            newRecord.PostCodeZip = d.PostCodeZip;
                            newRecord.TownCity = d.TownCity;
                            newRecord.EmailAddress = d.EmailAddress;
                            newRecord.LastStatus = Convert.ToInt16(d.NotEligibleDueToStatus);
                            newRecord.LastStatusMessage = d.NotEligibleDueToMessage;
                            newRecord.PhoneDaytime = d.PhoneDaytime;
                            newRecord.PhoneEvening = d.PhoneEvening;
                            newRecord.PolicyDateCreated = d.PolicyDateCreated;
                            newRecord.PolicyEndDate = d.PolicyEndDate;
                            newRecord.PolicyStartDate = d.PolicyStartDate;
                            newRecord.IsItPrimaryBarcode = true;
                            //add to the list
                            refusalData.Add(newRecord);
                        }
                    }

                    scope.Complete();
                    return refusalData;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("Master module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                    return null;
                }

            }
        }

        public bool insertDataToRefusal(RefusalData refusalData)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    RefusalEmailCampaign refData = new RefusalEmailCampaign();
                    refData.CertificateReference = refusalData.CertificateReference;
                    refData.TripTypeId = Convert.ToInt16(refusalData.TripTypeId);
                    refData.DateInserted = DateTime.Now;
                    refData.LastModified = DateTime.Now;
                    refData.IsCancelled = refusalData.IsCancelled;
                    refData.IsEligible = refusalData.IsEligible;
                    refData.IsPaid = refusalData.IsPaid;
                    refData.PromotionalCode = refusalData.PromotionalCode;
                    refData.Title = refusalData.Title;
                    refData.FirstName = refusalData.FirstName;
                    refData.LastName = refusalData.LastName;
                    refData.DateOfBirth = refusalData.DateOfBirth;
                    refData.Age = refusalData.Age;
                    refData.AddressLineOne = refusalData.AddressLineOne;
                    refData.AddressLineTwo = refusalData.AddressLineTwo;
                    refData.AddressLineThree = refusalData.AddressLineThree;
                    refData.CountyState = refusalData.CountyState;
                    refData.PostCodeZip = refusalData.PostCodeZip;
                    refData.TownCity = refusalData.TownCity;
                    refData.EmailAddress = refusalData.EmailAddress;
                    refData.LastStatus = Convert.ToInt16(refusalData.LastStatus);
                    refData.LastStatusMessage = refusalData.LastStatusMessage;
                    refData.PhoneDaytime = refusalData.PhoneDaytime;
                    refData.PhoneEvening = refusalData.PhoneEvening;
                    refData.PolicyDateCreated = refusalData.PolicyDateCreated;
                    refData.PolicyEndDate = refusalData.PolicyEndDate;
                    refData.PolicyStartDate = refusalData.PolicyStartDate;

                    myLocalContext.RefusalEmailCampaigns.InsertOnSubmit(refData);

                    var policyMasterData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == refusalData.CertificateReference && a.HasTheRefusalEmailBeenSent != true).FirstOrDefault();
                    if (policyMasterData != null)
                    {
                        policyMasterData.HasTheRefusalEmailBeenSent = true;
                        policyMasterData.LastModified = DateTime.Now;

                    }

                    myLocalContext.SubmitChanges();
                    scope.Complete();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        public bool insertPurePointDataToLocalTable(PurePrintModel purePrintRow)
        {
            try
            {
                PurePrintFeed dbObject = new PurePrintFeed();
                dbObject.CertificateReference = purePrintRow.CertificateReference;
                dbObject.Status = purePrintRow.Status;
                dbObject.Email = purePrintRow.Email;
                dbObject.AgentId = purePrintRow.AgentId;
                dbObject.AgentName = purePrintRow.AgentName;
                dbObject.MessageName = purePrintRow.MessageName;
                dbObject.Title = purePrintRow.Title;
                dbObject.FirstName = purePrintRow.FirstName;
                dbObject.LastName = purePrintRow.LastName;
                dbObject.CreatedDate = purePrintRow.DateCreated;
                dbObject.MasterPolicyUpdated = purePrintRow.MasterPolicyUpdated;
                dbObject.MasterPolicyUpdateDate = purePrintRow.MasterPolicyUpdatedDate;

                myLocalContext.PurePrintFeeds.InsertOnSubmit(dbObject);
                myLocalContext.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool updateLocalPurePointTableRow()
        {

            return true;
        }

        public List<MasterPolicy> getAllPolicyMasterData()
        {
            try
            {
                var myList = (from policyRow in myLocalContext.PolicyMasters where policyRow.HasBarcodeBeenAllocated==false
                              select new MasterPolicy
                              {
                                  CertificateReference = policyRow.CertificateReference,
                                  AgentId = policyRow.AgentId,
                                  AgentName = policyRow.AgentName,
                                  TripTypeId = policyRow.TripTypeId,
                                  PolicyDateCreated = policyRow.PolicyDateCreated,
                                  PolicyStartDate = policyRow.PolicyStartDate,
                                  PolicyEndDate = policyRow.PolicyEndDate,
                                  IsCancelled = policyRow.IsCancelled,
                                  IsPaid = policyRow.IsPaid,
                                  IsEligible = policyRow.IsEligible,
                                  PromotionalCode = policyRow.PromotionalCode,
                                  Title = policyRow.Title,
                                  FirstName = policyRow.FirstName,
                                  LastName = policyRow.LastName,
                                  DateOfBirth = policyRow.DateOfBirth,
                                  Age = policyRow.Age,
                                  AddressLineOne = policyRow.AddressLineOne,
                                  AddressLineTwo = policyRow.AddressLineTwo,
                                  AddressLineThree = policyRow.AddressLineThree,
                                  TownCity = policyRow.TownCity,
                                  CountyState = policyRow.CountyState,
                                  PostCodeZip = policyRow.PostCodeZip,
                                  PhoneDaytime = policyRow.PhoneDaytime,
                                  PhoneEvening = policyRow.PhoneEvening,
                                  EmailAddress = policyRow.EmailAddress,
                                  HasItBeenReplaced = policyRow.HasItBeenReplaced,
                                  DateInserted = policyRow.DateInserted,
                                  BarcodeNumber = policyRow.BarcodeNumber,
                                  BarcodeDisplay = policyRow.BarcodeDisplay,
                                  Pin = policyRow.Pin,
                                  Value = policyRow.Value,
                                  BarcodeStartDate = policyRow.BarcodeStartDate,
                                  BarcodeEndDate = policyRow.BarcodeEndDate,
                                  ImageDirectory = policyRow.ImageDirectory,
                                  LastModified = policyRow.LastModified,
                                  LastReplacementDate = policyRow.LastReplacementDate,
                                  LastStatus = policyRow.LastStatus,
                                  IsItFromMagenta = policyRow.IsItFromMagenta,
                                  HasValidEmail = policyRow.HasValidEmail,
                                  IsFiltered = policyRow.IsFiltered,
                                  LastStatusMessage = policyRow.LastStatusMessage,
                                  HasBarcodeBeenAllocated = policyRow.HasBarcodeBeenAllocated
                              }).ToList();

                if (myList.Count == 0) throw new Exception("There were a problem to select policies from PolicyMasterTable");
                return myList;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                return null;
            }

        }

        public List<MasterPolicy> getAllPolicyMasterDataReplace()
        {
            try
            {
                var myList = (from policyRow in myLocalContext.PolicyMasters
                              where policyRow.HasBarcodeBeenAllocated == true && policyRow.LastStatus==9
                              select new MasterPolicy
                              {
                                  CertificateReference = policyRow.CertificateReference,
                                  AgentId = policyRow.AgentId,
                                  AgentName = policyRow.AgentName,
                                  TripTypeId = policyRow.TripTypeId,
                                  PolicyDateCreated = policyRow.PolicyDateCreated,
                                  PolicyStartDate = policyRow.PolicyStartDate,
                                  PolicyEndDate = policyRow.PolicyEndDate,
                                  IsCancelled = policyRow.IsCancelled,
                                  IsPaid = policyRow.IsPaid,
                                  IsEligible = policyRow.IsEligible,
                                  PromotionalCode = policyRow.PromotionalCode,
                                  Title = policyRow.Title,
                                  FirstName = policyRow.FirstName,
                                  LastName = policyRow.LastName,
                                  DateOfBirth = policyRow.DateOfBirth,
                                  Age = policyRow.Age,
                                  AddressLineOne = policyRow.AddressLineOne,
                                  AddressLineTwo = policyRow.AddressLineTwo,
                                  AddressLineThree = policyRow.AddressLineThree,
                                  TownCity = policyRow.TownCity,
                                  CountyState = policyRow.CountyState,
                                  PostCodeZip = policyRow.PostCodeZip,
                                  PhoneDaytime = policyRow.PhoneDaytime,
                                  PhoneEvening = policyRow.PhoneEvening,
                                  EmailAddress = policyRow.EmailAddress,
                                  HasItBeenReplaced = policyRow.HasItBeenReplaced,
                                  DateInserted = policyRow.DateInserted,
                                  BarcodeNumber = policyRow.BarcodeNumber,
                                  BarcodeDisplay = policyRow.BarcodeDisplay,
                                  Pin = policyRow.Pin,
                                  Value = policyRow.Value,
                                  BarcodeStartDate = policyRow.BarcodeStartDate,
                                  BarcodeEndDate = policyRow.BarcodeEndDate,
                                  ImageDirectory = policyRow.ImageDirectory,
                                  LastModified = policyRow.LastModified,
                                  LastReplacementDate = policyRow.LastReplacementDate,
                                  LastStatus = policyRow.LastStatus,
                                  IsItFromMagenta = policyRow.IsItFromMagenta,
                                  HasValidEmail = policyRow.HasValidEmail,
                                  IsFiltered = policyRow.IsFiltered,
                                  LastStatusMessage = policyRow.LastStatusMessage,
                                  HasBarcodeBeenAllocated = policyRow.HasBarcodeBeenAllocated
                              }).ToList();

                if (myList.Count == 0) throw new Exception("There were a problem to select policies from PolicyMasterTable");
                return myList;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                return null;
            }

        }

        public MonthTracker checkWhetherProvidedMonthBarcodesExists(string year, string month)
        {
            try
            {
                var data = myLocalContext.GetTable<MonthTracker>().Where(a => a.year == year && a.month == month && a.imagesCreated == true && a.imagesPathsAllocated == true).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                return null;
            }
        }

        public MasterPolicy allocateBarcode(string year, string month, MasterPolicy filteredPolicy)
        {
            MasterData ms = new MasterData();
            var value = ms.isDebenhamsOfferScope(filteredPolicy.PolicyDateCreated.Value, filteredPolicy.TripTypeId.Value);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {

                    int tripType = Convert.ToInt16(filteredPolicy.TripTypeId);
                    int giftcardValue;
                    #region old Code
                    //if (tripType == 1)
                    //{
                    //    giftcardValue = 5;
                    //}
                    //else if (tripType == 2)
                    //{
                    //    giftcardValue = 10;
                    //}
                    //else
                    //{
                    //    throw new Exception("Policy " + filteredPolicy.CertificateReference + " has wrong trip type.\nCannot allocate the barcode...");
                    //}
                    #endregion old Code

                    if (tripType == 1)
                    {
                        giftcardValue = 5;
                        if (value != 0)
                        {
                            giftcardValue = value;
                            Console.WriteLine("Debenhams Offer Applied for Policy Number -" + filteredPolicy.CertificateReference);
                        }
                    }
                    else if (tripType == 2)
                    {
                        giftcardValue = 10;
                        if (value != 0)
                        {
                            giftcardValue = value;
                            Console.WriteLine("Debenhams Offer Applied for Policy Number -" + filteredPolicy.CertificateReference);
                        }
                    }
                    else
                    {
                        throw new Exception("Policy " + filteredPolicy.CertificateReference + " has wrong trip type.\nCannot allocate the barcode...");
                    }
                    //get unallocated the barcode from provided year and month with policy specified value
                    var data = myLocalContext.GetTable<barcode>().Where(a => a.DateCreated.Year.ToString() == year && a.DateCreated.Month.ToString() == month.TrimStart('0') && a.HasBeenAllocated == false && a.ImageDirectory != null && a.BarcodeDisplay != null && a.BarcodeNumber != null && a.Pin != null && a.Value == giftcardValue && a.StartDate != null && a.EndDate != null).FirstOrDefault();
                    if (data == null) { throw new Exception("Cannot get the valid barcode from valid year and month to allocate for " + filteredPolicy.CertificateReference); }
                    //allocate barcode to filtered policy

                    var policyData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == filteredPolicy.CertificateReference && a.IsFiltered == true && a.IsCancelled == false && a.IsEligible == true && a.HasBarcodeBeenAllocated == false).FirstOrDefault();
                    if (policyData == null) { return filteredPolicy; }

                    policyData.BarcodeDisplay = data.BarcodeDisplay;
                    filteredPolicy.BarcodeNumber = policyData.BarcodeNumber = data.BarcodeNumber;
                    filteredPolicy.Pin = policyData.Pin = data.Pin;
                    filteredPolicy.Value = policyData.Value = data.Value;
                    filteredPolicy.BarcodeStartDate = policyData.BarcodeStartDate = data.StartDate;
                    filteredPolicy.BarcodeEndDate = policyData.BarcodeEndDate = data.EndDate;
                    filteredPolicy.ImageDirectory = policyData.ImageDirectory = data.ImageDirectory;
                    filteredPolicy.HasBarcodeBeenAllocated = policyData.HasBarcodeBeenAllocated = true;
                    filteredPolicy.LastModified = policyData.LastModified = DateTime.Now;


                    //update barcode table - set flag HasBeenAllocated = true and allocation date
                    data.HasBeenAllocated = true;
                    data.AllocationDate = DateTime.Now;
                    myLocalContext.SubmitChanges();
                    //update policy table 



                    scope.Complete();
                    return filteredPolicy;
                }
                catch (Exception ex)
                {
                    //Don't need call any rollback like method
                    throw ex;

                }
            }
        }

        public bool allocatesActions(MasterPolicy policyRow)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //policy data
                    var policyData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == policyRow.CertificateReference && a.IsFiltered == true && a.IsCancelled == false && a.IsEligible == true && a.LastStatus == null && a.LastStatusMessage == null && a.HasItBeenReplaced == false).FirstOrDefault();
                    if (policyData == null) {
                        return true;
                    }
                    //pure360EmailCampaign data
                    if (policyData.HasValidEmail == true)
                    {
                        DebenhamsEmailCampaign data = new DebenhamsEmailCampaign();
                        data.CertificateReference = policyData.CertificateReference;
                        data.TripTypeId = Convert.ToInt16(policyData.TripTypeId);
                        data.PolicyDateCreated = Convert.ToDateTime(policyData.PolicyDateCreated);
                        data.PolicyStartDate = Convert.ToDateTime(policyData.PolicyStartDate);
                        data.PolicyEndDate = Convert.ToDateTime(policyData.PolicyEndDate);
                        data.Title = policyData.Title;
                        data.FirstName = policyData.FirstName;
                        data.LastName = policyData.LastName;
                        data.DateOfBirth = policyData.DateOfBirth;
                        data.Age = policyData.Age;
                        data.AddressLineOne = policyData.AddressLineOne;
                        data.AddressLineTwo = policyData.AddressLineTwo;
                        data.AddressLineThree = policyData.AddressLineThree;
                        data.TownCity = policyData.TownCity;
                        data.CountyState = policyData.CountyState;
                        data.PostCodeZip = policyData.PostCodeZip;
                        data.PhoneDaytime = policyData.PhoneDaytime;
                        data.PhoneEvening = policyData.PhoneEvening;
                        data.EmailAddress = policyData.EmailAddress;
                        data.DateInserted = policyData.DateInserted;
                        data.BarcodeNumber = policyData.BarcodeNumber;
                        data.BarcodeDisplay = policyData.BarcodeDisplay;
                        data.Pin = policyData.Pin;
                        data.Value = Convert.ToInt16(policyData.Value);
                        data.BarcodeStartDate = Convert.ToDateTime(policyData.BarcodeStartDate);
                        data.BarcodeEndDate = Convert.ToDateTime(policyData.BarcodeEndDate);
                        data.ImageDirectory = policyData.ImageDirectory;
                        data.LastModified = DateTime.Now;
                        myLocalContext.DebenhamsEmailCampaigns.InsertOnSubmit(data);

                        //LAST STATUS x 
                        //LAST STATUS_MESSAGE = "Gift Card has been sent by email"
                        //insert and update LastStatus code and LastStatusMessage
                        int lastStatus = 4;
                        string lastStatusMessage = "Gift Card has been sent by email";
                        policyData.LastStatus = lastStatus;
                        policyData.LastStatusMessage = lastStatusMessage;
                        policyData.LastModified = DateTime.Now;

                        BarcodeTracking barcodeHistory = new BarcodeTracking();
                        barcodeHistory.CertificateReference = policyData.CertificateReference;
                        barcodeHistory.BarcodeNumber = policyData.BarcodeNumber;
                        barcodeHistory.Status = lastStatus;
                        barcodeHistory.StatusMessage = lastStatusMessage;
                        barcodeHistory.LastModified = DateTime.Now;
                        barcodeHistory.CreatedDate = DateTime.Now;

                        myLocalContext.BarcodeTrackings.InsertOnSubmit(barcodeHistory);


                    }
                    else
                    {
                        //IF HAS_VALID_EMAIL == false
                        //LAST STATUS x 
                        //LAST STATUS_MESSAGE = "Gift Card has been sent by post"
                        //insert and update LastStatus code and LastStatusMessage
                        int lastStatus = 5;
                        string lastStatusMessage = "Gift card has been forwarded to Operations for posting";
                        policyData.LastStatus = lastStatus;
                        policyData.LastStatusMessage = lastStatusMessage;
                        policyData.LastModified = DateTime.Now;

                        BarcodeTracking barcodeHistory = new BarcodeTracking();
                        barcodeHistory.CertificateReference = policyData.CertificateReference;
                        barcodeHistory.BarcodeNumber = policyData.BarcodeNumber;
                        barcodeHistory.Status = lastStatus;
                        barcodeHistory.StatusMessage = lastStatusMessage;
                        barcodeHistory.LastModified = DateTime.Now;
                        barcodeHistory.CreatedDate = DateTime.Now;

                        myLocalContext.BarcodeTrackings.InsertOnSubmit(barcodeHistory);
                    }
                    myLocalContext.SubmitChanges();

                    scope.Complete();
                    return true;


                }
                catch (Exception ex)
                {
                    //Don't need call any rollback like method
                    throw ex;

                }
            }
        }

        public bool updateLastStatusAndStatusMessage(MasterPolicy policyRow)
        {
            try
            {
                var data = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == policyRow.CertificateReference).FirstOrDefault();
                data.CertificateReference = policyRow.CertificateReference;
                data.AgentId = policyRow.AgentId;
                data.AgentName = policyRow.AgentName;
                data.TripTypeId = policyRow.TripTypeId;
                data.PolicyDateCreated = policyRow.PolicyDateCreated;
                data.PolicyStartDate = policyRow.PolicyStartDate;
                data.PolicyEndDate = policyRow.PolicyEndDate;
                data.IsCancelled = policyRow.IsCancelled;
                data.IsPaid = policyRow.IsPaid;
                data.IsEligible = policyRow.IsEligible;
                data.PromotionalCode = policyRow.PromotionalCode;
                data.Title = policyRow.Title;
                data.FirstName = policyRow.FirstName;
                data.LastName = policyRow.LastName;
                data.DateOfBirth = policyRow.DateOfBirth;
                data.Age = policyRow.Age;
                data.AddressLineOne = policyRow.AddressLineOne;
                data.AddressLineTwo = policyRow.AddressLineTwo;
                data.AddressLineThree = policyRow.AddressLineThree;
                data.TownCity = policyRow.TownCity;
                data.CountyState = policyRow.CountyState;
                data.PostCodeZip = policyRow.PostCodeZip;
                data.PhoneDaytime = policyRow.PhoneDaytime;
                data.PhoneEvening = policyRow.PhoneEvening;
                data.EmailAddress = policyRow.EmailAddress;
                data.HasItBeenReplaced = policyRow.HasItBeenReplaced;
                data.DateInserted = policyRow.DateInserted;
                data.BarcodeNumber = policyRow.BarcodeNumber;
                data.BarcodeDisplay = policyRow.BarcodeDisplay;
                data.Pin = policyRow.Pin;
                data.Value = policyRow.Value;
                data.BarcodeStartDate = policyRow.BarcodeStartDate;
                data.BarcodeEndDate = policyRow.BarcodeEndDate;
                data.ImageDirectory = policyRow.ImageDirectory;
                data.LastModified = DateTime.Now;
                data.LastReplacementDate = policyRow.LastReplacementDate;
                data.LastStatus = policyRow.LastStatus;
                data.IsItFromMagenta = policyRow.IsItFromMagenta;
                data.HasValidEmail = policyRow.HasValidEmail;
                data.IsFiltered = true;
                data.LastStatusMessage = policyRow.LastStatusMessage;
                data.HasBarcodeBeenAllocated = policyRow.HasBarcodeBeenAllocated;

                myLocalContext.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool updatePure360Statuses(MasterPolicy policyRow)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //policy data
                    var policyData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == policyRow.CertificateReference).FirstOrDefault();
                    if (policyData == null) { throw new Exception("Policy " + policyRow.CertificateReference + " used to screen against any status information in pure360status table..."); }
                    //pure360EmailCampaign data

                    var purePrint = myLocalContext.GetTable<PurePrintFeed>().Where(a => a.CertificateReference == policyData.CertificateReference && a.MasterPolicyUpdated == false).FirstOrDefault();
                    if (purePrint == null)
                    {
                        return true;
                    }
                    else
                    {
                        //LAST STATUS x 
                        //LAST STATUS_MESSAGE = "Gift Card has been sent by email"
                        //insert and update LastStatus code and LastStatusMessage
                        int lastStatus = 11;
                        string lastStatusMessage = "Gift Card Email has been blocked/bounced";
                        policyData.LastStatus = lastStatus;
                        policyData.LastStatusMessage = lastStatusMessage;
                        policyData.LastModified = DateTime.Now;

                        BarcodeTracking barcodeHistory = new BarcodeTracking();
                        barcodeHistory.CertificateReference = policyRow.CertificateReference;
                        barcodeHistory.BarcodeNumber = policyRow.BarcodeNumber;
                        barcodeHistory.Status = lastStatus;
                        barcodeHistory.StatusMessage = lastStatusMessage;
                        barcodeHistory.LastModified = DateTime.Now;
                        barcodeHistory.CreatedDate = DateTime.Now;

                        purePrint.MasterPolicyUpdated = true;
                        purePrint.MasterPolicyUpdateDate = DateTime.Now;

                        myLocalContext.BarcodeTrackings.InsertOnSubmit(barcodeHistory);
                        myLocalContext.SubmitChanges();

                        scope.Complete();
                        return true;
                    }

                }
                catch (Exception ex)
                {
                    //Don't need call any rollback like method
                    throw ex;

                }
            }
        }

        public List<BarcodeReplace> getAllReplacementBarcodes()
        {
            try
            {
                var myList = (from g in myLocalContext.BarcodeReplacements
                              select new BarcodeReplace
                              {
                                  CertificateReference = g.CertificateReference,
                                  DateInserted = g.DateInserted,
                                  BarcodeNumber = g.BarcodeNumber,
                                  BarcodeDisplay = g.BarcodeDisplay,
                                  Pin = g.Pin,
                                  Value = g.Value,
                                  BarcodeStartDate = g.BarcodeStartDate,
                                  BarcodeEndDate = g.BarcodeEndDate,
                                  ImageDirectory = g.ImageDirectory,
                                  LastModified = g.LastModified,
                                  HasTheImageBeenAllocated = g.HasTheImageBeenAllocated,
                                  ImageAllocationDate = g.ImageAllocationDate,
                                  HasItBeenReplaced = g.HasItBeenReplaced,
                                  LastReplacementDate = g.LastReplacementDate,
                                  LastStatus = g.LastStatus,
                                  LastStatusMessage = g.LastStatusMessage,
                                  BarcodeAllocationDate = g.BarcodeAllocationDate,
                                  HasTheBarcodeBeenAllocated = g.HasTheBarcodeBeenAllocated
                              }).ToList();

                return myList;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                return null;
            }
        }

        public bool issueNewBarcodesForMasterPolicyTable(string year, string month, MasterPolicy filteredPolicy)
        {
            MasterData ms = new MasterData();
            var value = ms.isDebenhamsOfferScope(filteredPolicy.PolicyDateCreated.Value, filteredPolicy.TripTypeId.Value);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    int tripType = Convert.ToInt16(filteredPolicy.TripTypeId);
                    int giftcardValue;
                    if (tripType == 1)
                    {
                        giftcardValue = 5;
                        if (value != 0)
                        {
                            giftcardValue = value;
                            Console.WriteLine("Debenhams Offer Applied for Policy Number -" + filteredPolicy.CertificateReference);
                        }

                    }
                    else if (tripType == 2)
                    {
                        giftcardValue = 10;
                        if (value != 0)
                        {
                            giftcardValue = value;
                            Console.WriteLine("Debenhams Offer Applied for Policy Number -" + filteredPolicy.CertificateReference);
                        }
                    }
                    else
                    {
                        throw new Exception("Policy " + filteredPolicy.CertificateReference + " has wrong trip type.\nCannot allocate the barcode...");
                    }
                    //grab the barcode from year and month
                    var data = myLocalContext.GetTable<barcode>().Where(a => a.DateCreated.Year.ToString() == year && a.DateCreated.Month.ToString() == month && a.HasBeenAllocated == false && a.ImageDirectory != null && a.BarcodeDisplay != null && a.BarcodeNumber != null && a.Pin != null && a.Value == giftcardValue && a.StartDate != null && a.EndDate != null).FirstOrDefault();
                    if (data == null) { throw new Exception("Cannot get the valid barcode (replacement) from valid year and month to allocate for " + filteredPolicy.CertificateReference); }

                    //grab policyData from PolicyMaster table
                    var policyData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == filteredPolicy.CertificateReference && a.IsFiltered == true && a.IsCancelled == false && a.IsEligible == true && a.HasBarcodeBeenAllocated == true).FirstOrDefault();
                    if (policyData == null) { return false; }

                    int lastStatus = 10;
                    string lastStatusMessage = "Barcode has been replaced. It has been forwarded to operations for posting";

                    BarcodeReplacement barcodeReplace = new BarcodeReplacement();
                    barcodeReplace.BarcodeAllocationDate = DateTime.Now;
                    barcodeReplace.HasTheBarcodeBeenAllocated = true;
                    barcodeReplace.BarcodeDisplay = data.BarcodeDisplay;
                    barcodeReplace.BarcodeNumber = data.BarcodeNumber;
                    barcodeReplace.Value = data.Value;
                    barcodeReplace.Pin = data.Pin;
                    barcodeReplace.BarcodeStartDate = data.StartDate;
                    barcodeReplace.BarcodeEndDate = data.EndDate;
                    barcodeReplace.CertificateReference = policyData.CertificateReference;
                    barcodeReplace.DateInserted = DateTime.Now;
                    barcodeReplace.HasItBeenReplaced = false;
                    barcodeReplace.HasTheImageBeenAllocated = true;
                    barcodeReplace.ImageAllocationDate = DateTime.Now;
                    barcodeReplace.ImageDirectory = data.ImageDirectory;
                    barcodeReplace.LastStatus = lastStatus;
                    barcodeReplace.LastStatusMessage = lastStatusMessage;
                    myLocalContext.BarcodeReplacements.InsertOnSubmit(barcodeReplace);

                    //insert the row to barcodeTracking table
                    BarcodeTracking barcodeHistory = new BarcodeTracking();
                    barcodeHistory.CertificateReference = policyData.CertificateReference;
                    barcodeHistory.BarcodeNumber = data.BarcodeNumber;
                    barcodeHistory.Status = lastStatus;
                    barcodeHistory.StatusMessage = lastStatusMessage;
                    barcodeHistory.LastModified = DateTime.Now;
                    barcodeHistory.CreatedDate = DateTime.Now;
                    myLocalContext.BarcodeTrackings.InsertOnSubmit(barcodeHistory);


                    //update PolicyMaster table
                    policyData.LastModified = DateTime.Now;
                    policyData.HasItBeenReplaced = true;
                    policyData.LastReplacementDate = DateTime.Now;


                    data.HasBeenAllocated = true;
                    data.AllocationDate = DateTime.Now;

                    myLocalContext.SubmitChanges();

                    scope.Complete();
                    return true;
                }
                catch (Exception ex)
                {
                    //Don't need call any rollback like method
                    throw ex;

                }
            }


        }

        public bool issueNewBarcodesForBarcodeReplacementTable(string year, string month, BarcodeReplace barcodeRow)
        {

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //grab policyData from PolicyMaster table
                    var policyData = myLocalContext.GetTable<PolicyMaster>().Where(a => a.CertificateReference == barcodeRow.CertificateReference && a.IsFiltered == true && a.IsCancelled == false && a.IsEligible == true && a.HasBarcodeBeenAllocated == true).FirstOrDefault();
                    if (policyData == null) { return false; }


                    int tripType = Convert.ToInt16(policyData.TripTypeId);
                    int giftcardValue;
                    if (tripType == 1)
                    {
                        giftcardValue = 5;
                    }
                    else if (tripType == 2)
                    {
                        giftcardValue = 10;
                    }
                    else
                    {
                        throw new Exception("Policy " + policyData.CertificateReference + " has wrong trip type.\nCannot allocate the barcode...");
                    }
                    //grab the barcode from year and month
                    var data = myLocalContext.GetTable<barcode>().Where(a => a.DateCreated.Year.ToString() == year && a.DateCreated.Month.ToString() == month && a.HasBeenAllocated == false && a.ImageDirectory != null && a.BarcodeDisplay != null && a.BarcodeNumber != null && a.Pin != null && a.Value == giftcardValue && a.StartDate != null && a.EndDate != null).FirstOrDefault();
                    if (data == null) { throw new Exception("Cannot get the valid barcode (replacement) from valid year and month to allocate for " + policyData.CertificateReference); }



                    int lastStatus = 10;
                    string lastStatusMessage = "Barcode has been replaced. It has been forwarded to operations for posting";


                    //THIS is to issue replacement of the replacement
                    BarcodeReplacement replacementOfTheReplacement = new BarcodeReplacement();
                    replacementOfTheReplacement.BarcodeAllocationDate = DateTime.Now;
                    replacementOfTheReplacement.HasTheBarcodeBeenAllocated = true;
                    replacementOfTheReplacement.BarcodeDisplay = data.BarcodeDisplay;
                    replacementOfTheReplacement.BarcodeNumber = data.BarcodeNumber;
                    replacementOfTheReplacement.Value = data.Value;
                    replacementOfTheReplacement.Pin = data.Pin;
                    replacementOfTheReplacement.BarcodeStartDate = data.StartDate;
                    replacementOfTheReplacement.BarcodeEndDate = data.EndDate;
                    replacementOfTheReplacement.CertificateReference = policyData.CertificateReference;
                    replacementOfTheReplacement.DateInserted = DateTime.Now;
                    replacementOfTheReplacement.HasItBeenReplaced = false;
                    replacementOfTheReplacement.HasTheImageBeenAllocated = true;
                    replacementOfTheReplacement.ImageAllocationDate = DateTime.Now;
                    replacementOfTheReplacement.ImageDirectory = data.ImageDirectory;
                    replacementOfTheReplacement.LastStatus = lastStatus;
                    replacementOfTheReplacement.LastStatusMessage = lastStatusMessage;
                    myLocalContext.BarcodeReplacements.InsertOnSubmit(replacementOfTheReplacement);

                    //this it to update the replacement barcode data - set flag that the replacement of replacement has been issued
                    var replacementData = myLocalContext.GetTable<BarcodeReplacement>().Where(a => a.CertificateReference == barcodeRow.CertificateReference && a.HasItBeenReplaced == false).FirstOrDefault();
                    if (replacementData == null) { throw new Exception("Cannot get the valid barcode (replacement) from valid year and month to allocate for " + policyData.CertificateReference); }
                    replacementData.HasItBeenReplaced = true;
                    replacementData.LastReplacementDate = DateTime.Now;
                    replacementData.LastModified = DateTime.Now;

                    //insert the row to barcodeTracking table
                    BarcodeTracking barcodeHistory = new BarcodeTracking();
                    barcodeHistory.BarcodeNumber = data.BarcodeNumber;
                    barcodeHistory.CertificateReference = policyData.CertificateReference;
                    barcodeHistory.Status = lastStatus;
                    barcodeHistory.StatusMessage = lastStatusMessage;
                    barcodeHistory.LastModified = DateTime.Now;
                    barcodeHistory.CreatedDate = DateTime.Now;
                    myLocalContext.BarcodeTrackings.InsertOnSubmit(barcodeHistory);

                    data.AllocationDate = DateTime.Now;
                    data.HasBeenAllocated = true;
                    myLocalContext.SubmitChanges();

                    scope.Complete();
                    return true;
                }
                catch (Exception ex)
                {
                    //Don't need call any rollback like method
                    throw ex;

                }
            }

        }
        public int isDebenhamsOfferScope(DateTime OfferStartDate, int triptype)
        {
            int value = 0;
            var inScopeData = myLocalContext.GetTable<DebenhamsOffer>().Where(a => a.TriptypeId == triptype).ToList();
            if (inScopeData != null)
            {
                foreach (var d in inScopeData)
                {
                    if (OfferStartDate >= d.OfferStartDate && OfferStartDate < d.OfferEndDate)
                    {
                        value = d.GiftCardValue;
                    }
                }
            }

            return value;

        }
    }



}
