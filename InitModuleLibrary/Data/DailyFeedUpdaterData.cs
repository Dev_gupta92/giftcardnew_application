﻿using InitModuleLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InitModuleLibrary.EmailNotificationHandler;

namespace InitModuleLibrary.Data
{
    public class DailyFeedUpdaterData
    {
        public DailyFeedUpdaterData() { }
        
        GiftcardAppDataContext myLocalContext = new GiftcardAppDataContext();

        public List<MagentaPolicyFeed> getMagentaPolicyFeedPolicies()
        {
            try
            {
                DateTime twoWeeksAgo = DateTime.Today.AddDays(-15);
                DateTime fromTime = new DateTime(twoWeeksAgo.Year, twoWeeksAgo.Month, twoWeeksAgo.Day, 0, 0, 0, 0);
                DateTime toTime = new DateTime(twoWeeksAgo.Year, twoWeeksAgo.Month, twoWeeksAgo.Day, 23, 59, 59, 999);
                //implement flag HasItBeenProcessed and condition where HasItBeenProcessed == 0
                var myList = myLocalContext.GetTable<MagentaPolicyFeed>().Where(a => a.HasItBeenMoved == false).ToList();
                if (myList.Count == 0)
                {
                    throw new Exception("There were no valid policies to copy from MagentaPolicyFeed table from that day");

                }

                return myList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", ex.Message);
                return null;
            }
        }

        public List<OriginalPolicyFeed> getOriginalPolicyFeedPolicies()
        {
            try
            {
                //implement flag HasItBeenProcessed and condition where HasItBeenProcessed == 0
                var myList = myLocalContext.GetTable<OriginalPolicyFeed>().Where(a => a.HasItBeenMoved == false).ToList();
                if (myList.Count == 0)
                {
                    throw new Exception("There were no valid policies to copy from OriginalPolicyFeed table from that day");
                }
                return myList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", ex.Message);
                return null;
            }

        }

        public bool insertMagentaPoliciesIntoPolicyMasterTable(MagentaPolicyFeed policyRow)
        {
            try
            {
                PolicyMaster dbObject = new PolicyMaster();
                dbObject.CertificateReference = policyRow.CertificateReference;
                dbObject.AgentId = policyRow.AgentId;
                dbObject.AgentName = policyRow.AgentName;
                dbObject.TripTypeId = policyRow.TripTypeId;
                dbObject.PolicyDateCreated = policyRow.DateCreated;
                dbObject.PolicyStartDate = policyRow.PolicyStartDate;
                dbObject.PolicyEndDate = policyRow.PolicyEndDate;
                dbObject.IsCancelled = policyRow.IsCancelled;
                dbObject.IsPaid = policyRow.IsPaid;
                dbObject.IsEligible = false;
                dbObject.PromotionalCode = policyRow.PromotionalCode;
                dbObject.Title = policyRow.Title;
                dbObject.FirstName = policyRow.FirstName;
                dbObject.LastName = policyRow.LastName;
                dbObject.DateOfBirth = policyRow.DateOfBirth;
                dbObject.Age = policyRow.Age;
                dbObject.AddressLineOne = policyRow.AddressLineOne;
                dbObject.AddressLineTwo = policyRow.AddressLineTwo;
                dbObject.AddressLineThree = policyRow.AddressLineThree;
                dbObject.TownCity = policyRow.TownCity;
                dbObject.CountyState = policyRow.CountyState;
                dbObject.PostCodeZip = policyRow.PostCodeZip;
                dbObject.PhoneDaytime = policyRow.PhoneDaytime;
                dbObject.PhoneEvening = policyRow.PhoneEvening;
                dbObject.EmailAddress = policyRow.EmailAddress;
                dbObject.HasItBeenReplaced = false;
                dbObject.DateInserted = DateTime.Now;
                dbObject.IsItFromMagenta = true;
                dbObject.HasBarcodeBeenAllocated = false;
                dbObject.IsFiltered = false;
                dbObject.HasValidEmail = false;
                myLocalContext.PolicyMasters.InsertOnSubmit(dbObject);
                
                var data = myLocalContext.GetTable<MagentaPolicyFeed>().Where(a => a.CertificateReference == policyRow.CertificateReference).FirstOrDefault();
                data.HasItBeenMoved = true;
                data.LastModified = DateTime.Now;
                myLocalContext.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool insertOriginalPoliciesIntoPolicyMasterTable(OriginalPolicyFeed policyRow)
        {

            try
            {
                PolicyMaster dbObject = new PolicyMaster();
                dbObject.CertificateReference = policyRow.CertificateReference;
                dbObject.AgentId = policyRow.AgentId;
                dbObject.AgentName = policyRow.AgentName;
                dbObject.TripTypeId = policyRow.TripTypeId;
                dbObject.PolicyDateCreated = policyRow.DateCreated;
                dbObject.PolicyStartDate = policyRow.PolicyStartDate;
                dbObject.PolicyEndDate = policyRow.PolicyEndDate;
                dbObject.IsCancelled = policyRow.IsCancelled;
                dbObject.IsPaid = policyRow.IsPaid;
                dbObject.IsEligible = false;
                dbObject.PromotionalCode = policyRow.PromotionalCode;
                dbObject.Title = policyRow.Title;
                dbObject.FirstName = policyRow.FirstName;
                dbObject.LastName = policyRow.LastName;
                dbObject.DateOfBirth = policyRow.DateOfBirth;
                dbObject.Age = policyRow.Age;
                dbObject.AddressLineOne = policyRow.AddressLineOne;
                dbObject.AddressLineTwo = policyRow.AddressLineTwo;
                dbObject.AddressLineThree = policyRow.AddressLineThree;
                dbObject.TownCity = policyRow.TownCity;
                dbObject.CountyState = policyRow.CountyState;
                dbObject.PostCodeZip = policyRow.PostCodeZip;
                dbObject.PhoneDaytime = policyRow.PhoneDaytime;
                dbObject.PhoneEvening = policyRow.PhoneEvening;
                dbObject.EmailAddress = policyRow.EmailAddress;
                dbObject.HasItBeenReplaced = false;
                dbObject.DateInserted = DateTime.Now;
                dbObject.IsItFromMagenta = false;
                dbObject.HasBarcodeBeenAllocated = false;
                dbObject.IsFiltered = false;
                dbObject.HasValidEmail = false;
                myLocalContext.PolicyMasters.InsertOnSubmit(dbObject);
               
                
                var data = myLocalContext.GetTable<OriginalPolicyFeed>().Where(a => a.CertificateReference == policyRow.CertificateReference).FirstOrDefault();
                data.HasItBeenMoved = true;
                data.LastModified = DateTime.Now;
                myLocalContext.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
