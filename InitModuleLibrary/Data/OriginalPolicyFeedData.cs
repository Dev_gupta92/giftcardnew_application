﻿using InitModuleLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InitModuleLibrary.EmailNotificationHandler;
namespace InitModuleLibrary.Data
{
    public class OriginalPolicyFeedData
    {
        public OriginalPolicyFeedData() { }

        OriginalPolicyDataContext myContext = new OriginalPolicyDataContext();
        GiftcardAppDataContext myLocalContext = new GiftcardAppDataContext();
        public List<OriginalPolicy> getPolicies(List<string> allAgents)
        {
            DateTime now = DateTime.Now;
            try
            {
                DateTime twoWeeksAgo = DateTime.Today.AddDays(-15);
                Console.WriteLine("Date now is " + now + " Date two weeks ago is " + twoWeeksAgo);
               // DateTime fromTime = new DateTime(twoWeeksAgo.Year, twoWeeksAgo.Month, twoWeeksAgo.Day, 0, 0, 0, 0);
             //   DateTime toTime = new DateTime(twoWeeksAgo.Year, twoWeeksAgo.Month, twoWeeksAgo.Day, 23, 59, 59, 999);
                //This will be run first time manually to cover whole October, then process will be run daily
                DateTime fromTime = new DateTime(2018, 06, 02, 0, 0, 0, 0);
                DateTime toTime = new DateTime(2018, 07, 01, 23, 59, 59, 999);
                Console.WriteLine("From time is " + fromTime + " To Time is " + toTime);

                var myList = (from d in myContext.Policies
                              join e in myContext.PolicyHolders on d.PolicyHolderId equals e.PolicyHolderId
                              join f in myContext.Quotes on d.QuoteId equals f.QuoteId
                              join g in myContext.Agents on d.AgentId equals g.AgentId
                              where d.IsPaid == true && allAgents.Contains(d.AgentId.ToString()) && !d.PolicyNumber.StartsWith("GAD") && !d.PolicyNumber.StartsWith("GDT") && !d.PolicyNumber.StartsWith("JUB") 
                              && !d.PolicyNumber.StartsWith("HAL") && d.DateCreated >= fromTime && d.DateCreated <= toTime
                          
                              select new OriginalPolicy
                              {
                                  CertificateId = d.PolicyId.ToString(),
                                  CertificateReference = d.PolicyNumber,
                                  AgentId = d.AgentId.ToString(),
                                  AgentName = g.AgentName.ToString(),
                                  TripTypeId = d.TripTypeId,
                                  DateCreated = Convert.ToDateTime(d.DateCreated),
                                  PolicyStartDate = Convert.ToDateTime(d.PolicyStartDate),
                                  PolicyEndDate = Convert.ToDateTime(d.PolicyEndDate),
                                  IsCancelled = Convert.ToBoolean(d.IsCancelled),
                                  IsPaid = Convert.ToBoolean(d.IsPaid),
                                  PromotionalCode = f.PromotionalCode,
                                  Title = e.Title,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  DateOfBirth = e.DateOfBirth ?? null,
                                  Age = e.Age ?? null,
                                  AddressLineOne = e.AddressLineOne,
                                  AddressLineTwo = e.AddressLineTwo,
                                  AddressLineThree = e.AddressLineThree,
                                  TownCity = e.TownCity,
                                  CountyState = e.CountyState,
                                  PostCodeZip = e.PostcodeZip,
                                  PhoneDaytime = e.PhoneDaytime,
                                  PhoneEvening = e.PhoneEvening,
                                  EmailAddress = e.EmailAddress
                              }).ToList();

                Console.WriteLine("we have got our data");

                if (myList.Count == 0) {
                    throw new Exception("There were no debenhams policies to copy from Original policy table \nfrom that day ("+ twoWeeksAgo.Year+"/"+ twoWeeksAgo.Month + "/" + twoWeeksAgo.Day + ") (This might be not error - investigate it manually)");
                }
                Console.WriteLine(5);
                return myList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Original Policy Feed Exception", ex.Message);
                return null;
            }
        }

        public bool insertCopiedPolicies(OriginalPolicy tobeInserted)
        {
            DateTime now = DateTime.Now;
            try
            {
                OriginalPolicyFeed dbObject = new OriginalPolicyFeed();
                //dbObject.CertificateReference = tobeInserted.CertificateReference;
                dbObject.CertificateReference = tobeInserted.CertificateReference;
                dbObject.AgentId = tobeInserted.AgentId;
                dbObject.AgentName = tobeInserted.AgentName;
                dbObject.TripTypeId = tobeInserted.TripTypeId;
                dbObject.DateCreated = tobeInserted.DateCreated;
                dbObject.PolicyStartDate = tobeInserted.PolicyStartDate;
                dbObject.PolicyEndDate = tobeInserted.PolicyEndDate;
                dbObject.IsCancelled = tobeInserted.IsCancelled;
                dbObject.IsPaid = tobeInserted.IsPaid;
                dbObject.PromotionalCode = tobeInserted.PromotionalCode;
                dbObject.Title = tobeInserted.Title;
                dbObject.FirstName = tobeInserted.FirstName;
                dbObject.LastName = tobeInserted.LastName;
                dbObject.DateOfBirth = tobeInserted.DateOfBirth;
                dbObject.Age = tobeInserted.Age;
                dbObject.AddressLineOne = tobeInserted.AddressLineOne;
                dbObject.AddressLineTwo = tobeInserted.AddressLineTwo;
                dbObject.AddressLineThree = tobeInserted.AddressLineThree;
                dbObject.TownCity = tobeInserted.TownCity;
                dbObject.CountyState = tobeInserted.CountyState;
                dbObject.PostCodeZip = tobeInserted.PostCodeZip;
                dbObject.PhoneDaytime = tobeInserted.PhoneDaytime;
                dbObject.PhoneEvening = tobeInserted.PhoneEvening;
                dbObject.EmailAddress = tobeInserted.EmailAddress;
                dbObject.HasItBeenMoved = false;
                dbObject.DateInserted = DateTime.Now;
                myLocalContext.OriginalPolicyFeeds.InsertOnSubmit(dbObject);
                myLocalContext.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


    }
}
