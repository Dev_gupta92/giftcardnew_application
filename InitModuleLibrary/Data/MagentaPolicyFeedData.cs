﻿using InitModuleLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitModuleLibrary.Data
{
    class MagentaPolicyFeedData
    {
        public MagentaPolicyFeedData() { }

        GiftcardAppDataContext myContext = new GiftcardAppDataContext();

        public bool insertPolicy(MagentaPolicy policyRow)
        {
            try
            {
                MagentaPolicyFeed dbObject = new MagentaPolicyFeed();
                dbObject.CertificateReference = policyRow.CertificateReference;
                dbObject.AgentId = policyRow.AgentId;
                dbObject.TripTypeId = policyRow.TripTypeId;
                dbObject.AgentName = policyRow.AgentName;
                dbObject.DateCreated = policyRow.DateCreated;
                dbObject.PolicyStartDate = policyRow.PolicyStartDate;
                dbObject.PolicyEndDate = policyRow.PolicyEndDate;
                dbObject.IsCancelled = policyRow.IsCancelled;
                dbObject.IsPaid = policyRow.IsPaid;
                dbObject.PromotionalCode = policyRow.PromotionalCode;
                dbObject.Title = policyRow.Title;
                dbObject.FirstName = policyRow.FirstName;
                dbObject.LastName = policyRow.LastName;
                dbObject.DateOfBirth = policyRow.DateOfBirth;
                dbObject.Age = policyRow.Age;
                dbObject.AddressLineOne = policyRow.AddressLineOne;
                dbObject.AddressLineTwo = policyRow.AddressLineTwo;
                dbObject.AddressLineThree = policyRow.AddressLineThree;
                dbObject.TownCity = policyRow.TownCity;
                dbObject.CountyState = policyRow.CountyState;
                dbObject.PostCodeZip = policyRow.PostCodeZip;
                dbObject.PhoneDaytime = policyRow.PhoneDaytime;
                dbObject.PhoneEvening = policyRow.PhoneEvening;
                dbObject.EmailAddress = policyRow.EmailAddress;
                dbObject.HasItBeenMoved = false;
                dbObject.DateInserted = DateTime.Now;
                
                myContext.MagentaPolicyFeeds.InsertOnSubmit(dbObject);

                myContext.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

      


    }
}
