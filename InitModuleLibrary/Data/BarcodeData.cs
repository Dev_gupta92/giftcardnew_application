﻿using InitModuleLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InitModuleLibrary;

namespace InitModuleLibrary.Data
{
    public class BarcodeData
    {
        public BarcodeData() { }

        GiftcardAppDataContext myContext = new GiftcardAppDataContext();

        public bool insertBarcode(Barcode barcodeRow) {
            try
            {
                barcode dbObject = new barcode();
                dbObject.BarcodeNumber = barcodeRow.BarcodeNumber;
                dbObject.BarcodeDisplay = barcodeRow.BarcodeDisplay;
                dbObject.Pin = barcodeRow.Pin;
                dbObject.Value = barcodeRow.Value;
                dbObject.StartDate = barcodeRow.StartDate;
                dbObject.EndDate = barcodeRow.EndDate;
                dbObject.DateCreated = DateTime.Now;
                myContext.barcodes.InsertOnSubmit(dbObject);
                myContext.SubmitChanges();
                return true;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool updateBarcode(barcode barcodeRow, int value, int year , int month) {
            try
            {
                var data = myContext.GetTable<barcode>().Where(a => a.BarcodeNumber == barcodeRow.BarcodeNumber && a.Value == value && a.DateCreated.Year == year && a.DateCreated.Month == month).FirstOrDefault();
                data.ImageDirectory = barcodeRow.ImageDirectory;
                data.LastModified = DateTime.Now;
                myContext.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<barcode> getBarcodes(int month, int year, int tripType) {
            var myList = new List<barcode>() { };

            #region old Code
            //if (tripType == 1)
            //{
            //    int STvalue = 5;
            //    myList = myContext.GetTable<barcode>().Where(a => a.DateCreated.Month == month && a.DateCreated.Year == year && a.Value == STvalue).ToList<barcode>(); 
            //}
            //else if (tripType == 2)
            //{
            //    int AMTvalue = 10;
            //    myList = myContext.GetTable<barcode>().Where(a => a.DateCreated.Month == month && a.DateCreated.Year == year && a.Value == AMTvalue).ToList<barcode>(); 

            //}
            #endregion old Code

            int STvalue = 5;
            int AMTvalue = 10;
            int Newvalue = 15;
            int NewAMTvalue = 20;
            if (tripType == 1)
            {
                myList = myContext.GetTable<barcode>().Where(a => a.DateCreated.Month == month && a.DateCreated.Year == year && (a.Value == STvalue || a.Value == AMTvalue)).ToList<barcode>();
            }
            else if (tripType == 2)
            {

                myList = myContext.GetTable<barcode>().Where(a => a.DateCreated.Month == month && a.DateCreated.Year == year && (a.Value == AMTvalue || a.Value == Newvalue || a.Value == NewAMTvalue)).ToList<barcode>();

            }
            return myList;

        }

    }
}
