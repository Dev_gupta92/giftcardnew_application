﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InitModuleLibrary;
using InitModuleLibrary.EmailNotificationHandler;

namespace InitModuleLibrary.Data
{
   public class MonthTrackerData
    {
        public MonthTrackerData() { }

        GiftcardAppDataContext myContext = new GiftcardAppDataContext();

        #region getMonthTrackerbyMonthAndYear
        public MonthTracker getMonthTrackerbyMonthAndYear(string month, string year)
        {
            try
            {
                var data = myContext.GetTable<MonthTracker>().Where(a => a.year == year && a.month == month).FirstOrDefault();
                return data;
            }
            catch (Exception ex) {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("BarcodeProcessor module exception", "Conn"+myContext.Connection.ConnectionString+" Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return null;
            }
            
        }
        #endregion

        #region setHasBeenInsertedToTrue
        public bool setHasBeenInsertedToTrue(string month, string year)
        {
            try { 
                MonthTracker myObject = new MonthTracker();
                myObject.barcodeCreatedDate = DateTime.Now;
                myObject.month = month;
                myObject.year = year;
                myContext.MonthTrackers.InsertOnSubmit(myObject);
                myContext.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("BarcodeProcessor module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;
            }
        }
        #endregion

        #region setImagesCreatedToTrue
        public bool setImagesCreatedToTrue(string month, string year)
        {
            try
            {
                var data = myContext.GetTable<MonthTracker>().Where(a => a.year == year && a.month == month).FirstOrDefault();
                data.imagesCreated = true;
                data.imagesCreatedDate = DateTime.Now;
                myContext.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageGenerator module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;
            }
        }
        #endregion

        #region checkIfThisMonthImagesPathsBeenAllocated
        public bool checkIfThisMonthImagesPathsBeenAllocated(string month, string year) {
            try
            {
                var data = myContext.GetTable<MonthTracker>().Where(a => a.year == year && a.month == month).FirstOrDefault();

                if (data.imagesPathsAllocated == true)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageGenerator module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;
            }
        }
        #endregion

        #region setThisMonthImagesPathsAllocatedToTrue
        public bool setThisMonthImagesPathsAllocatedToTrue(string month, string year)
        {
            try
            {
                var data = myContext.GetTable<MonthTracker>().Where(a => a.year == year && a.month == month).FirstOrDefault();
                data.imagesPathsAllocated = true;
                data.imagesPathsAllocatedDate = DateTime.Now;
                myContext.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Image Generator module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;
            }
        }
        #endregion
    }
}
