﻿using InitModuleLibrary.EmailNotificationHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitModuleLibrary.Data
{
    class MagentaDayTrackerData
    {
        public MagentaDayTrackerData() { }

        GiftcardAppDataContext myContext = new GiftcardAppDataContext();

        #region hasThisDayBeenProcessed
        public MagentaDayTracker hasThisDayBeenProcessed(string year, string month, string day)
        {
            try
            {
                var data = myContext.GetTable<MagentaDayTracker>().Where(a => a.year == year && a.month == month && a.day == day).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("ImageProcessor module exception", ex.Message);
                return null;
            }

        }
        #endregion

        #region setThisDayHasBeenProcessedToTrue
        public bool setThisDayHasBeenProcessedToTrue(string year, string month, string day)
        {
            try
            {
                MagentaDayTracker myObject = new MagentaDayTracker();
                myObject.dateCreated = DateTime.Now;
                myObject.month = month;
                myObject.year = year;
                myObject.day = day;
                myContext.MagentaDayTrackers.InsertOnSubmit(myObject);
                myContext.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
