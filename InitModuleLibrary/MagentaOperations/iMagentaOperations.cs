﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.IO;
using InitModuleLibrary.Model;
using InitModuleLibrary.Data;

namespace InitModuleLibrary.MagentaOperations 
{
    interface iMagentaOperations
    {
        
        string SFTPMagentaHost     { get; set; }
        string SFTPMagentaUser     { get; set; }
        string SFTPMagentaPass     { get; set; }
        string MagentaDir { get; set; }
        MemoryStream MagentaFileMem { get; set; }
        
        bool connectAndGrabTheFile(DateTime processDate, bool checkPrevDay);
        bool hasThisDayBeenProcessed(DateTime processDate, bool checkPrevDay);
        bool setThisDayHasBeenProcessedToTrue(DateTime processDate, bool setPrevDay);
        bool processFile();
        bool insertFilesToDatabase();
        bool checkFileInTheStream();
        bool validateFileRow(string[] d);
    }
}
