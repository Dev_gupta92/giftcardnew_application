﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Configuration;
using System.IO;
using InitModuleLibrary.Validations;
using InitModuleLibrary.EmailNotificationHandler;
using System.Collections;
using InitModuleLibrary.Model;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using InitModuleLibrary.Data;
using InitModuleLibrary;
using System.Drawing;
using TECIT.TBarCode;
namespace InitModuleLibrary.MagentaOperations
{
    public class MagentaOperations : iMagentaOperations 
    {
        private MemoryStream _fileMem = null;
        private List<Model.MagentaPolicy> _MagentaFileRows = null;
        private string _magentaHost = ConfigurationManager.AppSettings["STFPMagentaHost"];
        private string _magentaUser = ConfigurationManager.AppSettings["STFPMagentaUser"];
        private string _magentaPass = ConfigurationManager.AppSettings["STFPMagentaPass"];
        private string _magentaDirectory = ConfigurationManager.AppSettings["MagentaDir"];
        private string[] _magentaFileRowsStringified ;
        public string[] MagentaFileRowsStringified
        {
            get
            {
                return _magentaFileRowsStringified;
            }

            set
            {
                _magentaFileRowsStringified = value;
            }
        }


        public string MagentaDir
        {
            get
            {
                return _magentaDirectory;
            }

            set
            {
                _magentaDirectory = value;
            }
        }
        

        public string SFTPMagentaHost
        {
            get
            {
                return _magentaHost;
            }

            set
            {
                _magentaHost = value;
            }
        }

        public string SFTPMagentaUser
        {
            get
            {
                return _magentaUser;
            }

            set
            {
                _magentaUser = value;
            }
        }

        public string SFTPMagentaPass
        {
            get
            {
                return _magentaPass;
            }

            set
            {
                _magentaPass = value;
            }
        }

        public MemoryStream MagentaFileMem
        {
            get
            {
                return _fileMem;
            }

            set
            {
                _fileMem = value;
            }
        }
        


       #region connectAndGrabTheFile
        public bool connectAndGrabTheFile(DateTime processDate, bool checkPrevDay) {
       
            using (var client = new SftpClient(SFTPMagentaHost, SFTPMagentaUser, SFTPMagentaPass))
            {
                try
                {
                    client.Connect();
                    if (!client.IsConnected)
                    {
                        throw new Exception("MagentaFeed: Cannot connect to SFTP server");
                    }

                    if (!client.Exists(MagentaDir))
                    {
                        throw new Exception("MagentaFeed: SFTP directory does not exist");
                    }

                    var myFiles = client.ListDirectory(MagentaDir);
                    Validation validator = new Validation();

                    if (validator.magentaFileExists(myFiles, client, processDate,checkPrevDay))
                    {
                        validator.MagentaFileStringify();
                        if (!validator.isMagentaHeadingValid())
                        {
                           throw new Exception("There was some undefined problem with Magenta file headings");
                        }
                        MagentaFileMem = validator.MagentaFileMem;
                        MagentaFileRowsStringified = validator.MagentaFileRowsStringified;
                    }
                    else
                    {
                        throw new Exception("There are no files in the directory");
                    }
                    Console.WriteLine("File has been downloaded into the stream");
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("MagentaFeed module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                    return false;
                }

            }
        }


        #endregion

       #region hasThisDayBeenProcessed
        public bool hasThisDayBeenProcessed(DateTime processDate, bool checkPrevDay)
        {
            string year = processDate.Year.ToString();
            string month = processDate.ToString("MM");
            string day;
            if (checkPrevDay)
            {
                day = processDate.AddDays(-1).ToString("dd");
            }
            else
            {
                day = processDate.ToString("dd");
            }
            var mt = new MagentaDayTrackerData();
            if (mt.hasThisDayBeenProcessed(year, month, day) != null)
            {
                Console.WriteLine("This day (by default yesterday) has been already processed");
                return true;
            }
            Console.WriteLine("This day have not been proceeded yet");
            return false;
        }
        #endregion

       #region setThisDayHasBeenProcessedToTrue
        public bool setThisDayHasBeenProcessedToTrue(DateTime processDate, bool setPrevDay)
        {
            string year = processDate.Year.ToString();
            string month = processDate.ToString("MM");
            string day;
            if (setPrevDay)
            {
                day = processDate.AddDays(-1).ToString("dd");
            }
            else {
                day = processDate.ToString("dd");
            }

            var mt = new MagentaDayTrackerData();
            if (mt.setThisDayHasBeenProcessedToTrue(year, month, day) == true)
            {
                Console.WriteLine("The flag HasBeenProcessed has been set to true succesfully");
                return true;
            }
            Console.WriteLine("There was a problem with updating HasBeenProcessed flag to true for that day");
            EmailHandler emailHandler = new EmailHandler();
            emailHandler.sendEmailWithExceptionOrError("Magenta Feed module exception", "There was a problem with updating HasBeenProcessed flag to true for that day");
            return false;
        }
        #endregion
        
       #region processFiles
        public bool processFile()
        {
            try {
                if (!checkFileInTheStream()) throw new Exception("There is no file in the memory stream");
                //MAGENTA FILE INSERTING TO DATABASE
                 List<MagentaPolicy> magentaPolicies = new List<MagentaPolicy>();
                List<Exception> MagentaFeedExceptions = new List<Exception>();
                foreach (var data in MagentaFileRowsStringified.Skip(1))
                {
                    var policy = new MagentaPolicy();
                   
                    var d = data.Split(new char[] { ',' }).ToArray();
                    if (validateFileRow(d))
                    {
                        try
                        {
                            
                            //CertificateReference
                            policy.CertificateReference = d[1];
                            //AgentId
                            policy.AgentId = d[2];
                            //AgentName
                            policy.AgentName = d[3];
                            //TripTypeId
                            policy.TripTypeId = Convert.ToInt16(d[4]);
                            //DateCreated 
                            policy.DateCreated = Convert.ToDateTime(d[5]);
                            //PolicyStartDate
                            policy.PolicyStartDate = Convert.ToDateTime(d[6]);
                            //PolicyEndDate
                            policy.PolicyEndDate = Convert.ToDateTime(d[7]);
                            //IsCancelled
                            policy.IsCancelled = Convert.ToBoolean(Convert.ToInt16(d[8]));
                            //IsPaid
                            policy.IsPaid = true;
                            //PromotionalCode
                            policy.PromotionalCode = d[9];
                            //Title
                            policy.Title = d[10];
                            //First Name
                            policy.FirstName = d[11];
                            //Last Name 
                            policy.LastName = d[12];
                            //DateOfBirth
                            try
                            {
                                policy.DateOfBirth = Convert.ToDateTime(d[13]);

                            }
                            catch (Exception) {
                                policy.DateOfBirth = null;
                            }
                           //Age
                            int number1 = 0;
                            policy.Age = (int.TryParse(d[14], out number1)) ? number1 : 0;
                            //AddressLineOne
                            policy.AddressLineOne = d[15];
                            //AddressLineTwo 
                            policy.AddressLineTwo = d[16];
                            //AddressLineThree
                            policy.AddressLineThree = d[17];
                            //TownCity
                            policy.TownCity = d[18];
                            //CountyState
                            policy.CountyState = d[19];
                            //PostCodeZip 
                            policy.PostCodeZip = d[20];
                            //PhoneDaytime   
                            policy.PhoneDaytime = d[21];
                            //PhoneEvening    
                            policy.PhoneEvening = d[22];
                            //EmailAddress    
                            policy.EmailAddress = d[23];
                            
                            //adding policy to policies list
                            magentaPolicies.Add(policy);
                        }
                        catch (Exception ex) {
                            MagentaFeedExceptions.Add(ex);
                        }
                       
                    }
                }
                int allRows = MagentaFileRowsStringified.Skip(1).Count();
                int successful = allRows - MagentaFeedExceptions.Count();
                //VALIDATION
               if (magentaPolicies.Count == 0) throw new Exception("MagentaFeed: File does not contain valid data");

                _MagentaFileRows = magentaPolicies;
                Console.WriteLine("There were {0} rows of policies to allocate to MagentaPolicy model. \nResult: {1} out of {0} were allocated correctly.\n", allRows, successful);
                
                if (MagentaFeedExceptions.Count > 0)
                {
                    string message = "MagentaFeed: There was a problem with assigning rows to model.\nThere was " + allRows + " rows. \n" + MagentaFeedExceptions.Count + " were not inserted.";
                    Console.WriteLine(message);
                    string additionalMessage = "\nThere were following exceptions: \n";
                    foreach (var d in MagentaFeedExceptions)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    throw new Exception(message + "\n" + additionalMessage);
                }
                

                //ASSIGNING VALUES TO PRIVATE PROPERTY
              
                return true;
            }
            catch (Exception ex) {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MagentaPolicyFeed Exception ", ex.Message);
                if (ex.Message == "MagentaFeed: File does not contain valid data") {
                    return false;
                }
                return true;
            }
        }


        #endregion

   

        #region checkFileInTheStream
        public bool checkFileInTheStream()
        {
            if (MagentaFileMem != null)
            {
                return true;
            }
            return false;
        }
        #endregion

      
        #region validateFileRow
        public bool validateFileRow(string[] d)
        {
            //d[1] field is required = certificateReference 
            try
            {
               if (String.IsNullOrWhiteSpace(d[1]))
                 {
                    return false;
                 }
                
              
            }
            catch (Exception ex) {
                return false;
            }
            return true;
        }
        #endregion
 

      #region insertFilesToDatabase
      public bool insertFilesToDatabase()
      {
          try
          {
              //FILE 1
              var myPolicies = _MagentaFileRows;
              List<Exception> MagentaInsertExceptionList = new List<Exception>();
              foreach (var policyRow in myPolicies)
              {
                  try
                  {
                      var bc = new MagentaPolicyFeedData();
                      bc.insertPolicy(policyRow);
                  }
                  catch (Exception ex)
                  {
                        MagentaInsertExceptionList.Add(ex);
                  }
              }
          
                    int successful = (myPolicies.Count - MagentaInsertExceptionList.Count);
                    Console.WriteLine("There were {0} rows of Magenta policies to be inserted to database. \nResult: {1} out of {0} were inserted correctly", myPolicies.Count, successful);
             

               if (MagentaInsertExceptionList.Count > 0)
                {
                    string message = "There was a problem with insering the rows to Database from MagentaFeed csv file \nThere was " + myPolicies.Count + " rows. \n" + MagentaInsertExceptionList.Count + " rows were not inserted \n";

                    string additionalMessage = "\nThere were following exceptions:\n";
                    foreach (var d in MagentaInsertExceptionList)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("MagentaPolicyFeed Process Exception", message + "\n" + additionalMessage);
                    return false;
                }


                return true;
          }
          catch (Exception ex) {
              Console.WriteLine("There was a problem with insering the rows to Database from MagentaFeed csv file");
              EmailHandler emailHandler = new EmailHandler();
              emailHandler.sendEmailWithExceptionOrError("MagentaFeed module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
              return false;
          }
         
      }
        #endregion

     
    }



}

