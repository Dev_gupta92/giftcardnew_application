﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitModuleLibrary.Model
{
    public struct  PurePrintModel
    {
        public string CertificateReference { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public string AgentId { get; set; }
        public string AgentName { get; set; }
        public string MessageName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateCreated { get; set; }
        public bool MasterPolicyUpdated { get; set; }
        public DateTime? MasterPolicyUpdatedDate { get; set; }
   
    }
}
