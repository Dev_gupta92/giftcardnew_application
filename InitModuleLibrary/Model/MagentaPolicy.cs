﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitModuleLibrary.Model
{
    public struct MagentaPolicy
    {
        public string CertificateReference { get; set; }
        public string AgentId { get; set; }
        public string AgentName { get; set; }
        public int TripTypeId { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public bool? IsCancelled { get; set; }
        public bool? IsPaid { get; set; }
        public string PromotionalCode { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string TownCity { get; set; }
        public string CountyState { get; set; }
        public string PostCodeZip { get; set; }
        public string PhoneDaytime { get; set; }
        public string PhoneEvening { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? DateInserted { get; set; }
        public bool HasItBeenMoved { get; set; }
        public DateTime? LastModified { get; set; }
        // public bool HasBeenReplaced { get; set; }
        //  public string ReplacementDate { get; set; }

    }
}
