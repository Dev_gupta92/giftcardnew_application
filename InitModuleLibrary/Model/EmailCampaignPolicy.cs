﻿using System;

namespace InitModuleLibrary.Model
{
    public struct EmailCampaignPolicy
    {
        public string CertificateReference { get; set; }
        public int TripTypeId { get; set; }
        public DateTime? PolicyDateCreated { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string TownCity { get; set; }
        public string CountyState { get; set; }
        public string PostCodeZip { get; set; }
        public string PhoneDaytime { get; set; }
        public string PhoneEvening { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? DateInserted { get; set; }
        public string BarcodeNumber { get; set; }
        public string BarcodeDisplay { get; set; }
        public string Pin { get; set; }
        public int? Value { get; set; }
        public DateTime? BarcodeStartDate { get; set; }
        public DateTime? BarcodeEndDate { get; set; }
        public string ImageDirectory { get; set; }
        public DateTime? LastModified { get; set; }

    }
}
