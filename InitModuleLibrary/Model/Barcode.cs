﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitModuleLibrary.Model
{
    public struct  Barcode
    {
       
        public string BarcodeNumber { get; set; }
        public string BarcodeDisplay { get; set; }
        public string Pin { get; set; }
        public int  Value{ get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateCreated { get; set; }

    }
}
