﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitModuleLibrary.Model
{
    public class BarcodeReplace
    {
        public string CertificateReference { get; set; }
        public DateTime? DateInserted { get; set; }
        public string BarcodeNumber { get; set; }
        public string BarcodeDisplay { get; set; }
        public string Pin { get; set; }
        public int? Value { get; set; }
        public DateTime? BarcodeStartDate { get; set; }
        public DateTime? BarcodeEndDate { get; set; }
        public string ImageDirectory { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? HasTheImageBeenAllocated { get; set; }
        public DateTime? ImageAllocationDate { get; set; }
        public bool? HasItBeenReplaced { get; set; }
        public DateTime? LastReplacementDate { get; set; }
        public int? LastStatus { get; set; }
        public string LastStatusMessage { get; set; }
        public DateTime? BarcodeAllocationDate { get; set; }
        public bool? HasTheBarcodeBeenAllocated { get; set; }
    }
}
