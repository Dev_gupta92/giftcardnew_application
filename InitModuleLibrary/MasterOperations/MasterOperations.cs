﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Configuration;
using InitModuleLibrary.Model;
using InitModuleLibrary.Data;
using InitModuleLibrary.EmailNotificationHandler;
using System.Text.RegularExpressions;

namespace InitModuleLibrary.MasterOperations
{
    public class MasterOperations : iMasterOperations
    {

        private List<MasterPolicy> _masterPolicies = null;
        private List<MasterPolicy> _filteredMasterDataList = new List<MasterPolicy>();
        private List<MasterPolicy> _filteredWithAllocatedBarcodesMasterDataList = new List<MasterPolicy>();
        private List<BarcodeReplace> _barcodesReplacementsList = new List<BarcodeReplace>();
        public List<BarcodeReplace> BarcodeReplacementList
        {
            get
            {
                return _barcodesReplacementsList;
            }

            set
            {
                _barcodesReplacementsList = value;
            }
        }
        public List<MasterPolicy> masterPolicies
        {
            get
            {
                return _masterPolicies;
            }

            set
            {
                _masterPolicies = value;
            }
        }
        public List<MasterPolicy> filteredMasterDataList
        {
            get
            {
                return _filteredMasterDataList;
            }

            set
            {
                _filteredMasterDataList = value;
            }
        }
        public List<MasterPolicy> filteredWithAllocatedBarcodesMasterDataList
        {
            get
            {
                return _filteredWithAllocatedBarcodesMasterDataList;
            }

            set
            {
                _filteredWithAllocatedBarcodesMasterDataList = value;
            }
        }

        private List<RefusalData> _refusalDataList;

        public List<RefusalData> RefusalDataList
        {
            get
            {
                return _refusalDataList;
            }

            set
            {
                _refusalDataList = value;
            }
        }

        private List<PurePrintModel> _purePrintData;

        public List<PurePrintModel> purePrintData
        {
            get
            {
                return _purePrintData;
            }

            set
            {
                _purePrintData = value;
            }
        }

        public List<string> getAllAgents() { 
            var allAgents = ConfigurationManager.AppSettings["allAgents"].Split(';').ToList();
            return allAgents;
        }

        public bool getPurePointDataFromOriginalTable() {

            try
            {

        
                MasterData masterContext = new MasterData();
             
                var listDb = masterContext.getPurePointDataFromOriginalTable();
                if (listDb == null)
                {
                    return false;
                }
                purePrintData = listDb;
                Console.WriteLine("Email data from original PurePoint has been copied");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                return false;
            }
        }
        
        public bool insertPurePointDataToLocalTable()
        {

            try
            {
                if (purePrintData.Count == 0)
                {
                    return false;
                }
                var orgDataContext = new MasterData();
                List<Exception> insertedExceptions = new List<Exception>();
                foreach (var d in purePrintData) {
                    try
                    {
                        orgDataContext.insertPurePointDataToLocalTable(d);
                    }
                    catch (Exception ex) {
                        insertedExceptions.Add(ex);
                    }
                  
                }
                if (insertedExceptions.Count == 0) {
                    Console.WriteLine("All email data has been copied from original PurePoint table and inserted to PurePrintFeed table");
                    return true;
                }
                else
                {
                    Console.WriteLine("There was a problem with inserting pure point data to PurePrintFeed table");
                    
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions) {
                        listOfNotInsertedRows += "\n"+ e.Message +"\n";
                    }
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("Original Policy Feed Exception", "There were "+ purePrintData.Count+" policies.\n"+insertedExceptions.Count+" were not inserted.\nPlease investigate them manually"+ listOfNotInsertedRows);
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;
            }
          
        }
        
        public bool getAllPolicyMasterData() {
            try
            {
                var MasterDataContext = new MasterData();
                var listDb = MasterDataContext.getAllPolicyMasterData();
                if (listDb == null)
                {
                    return false;
                }
                masterPolicies = listDb;
                Console.WriteLine("All policies from PolicyMaster table have been selected");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Original Policy Feed Exception", ex.Message);
                return false;
            }

        }


        public bool getAllPolicyMasterDataReplace()
        {
            try
            {
                var MasterDataContext = new MasterData();
                var listDb = MasterDataContext.getAllPolicyMasterDataReplace();
                if (listDb == null)
                {
                    return false;
                }
                masterPolicies = listDb;
                Console.WriteLine("All policies from PolicyMaster table have been selected");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("Original Policy Feed Exception", ex.Message);
                return false;
            }

        }

        public string[] getStaffPromotionalCodes() {
            return ConfigurationManager.AppSettings["staffPromotionalCodes"].Split(';');
        }

        public string[] getInEligibleAgentsMagenta()
        {
            return ConfigurationManager.AppSettings["inEligibleAgentsMagenta"].Split(';');
        }

        public string[] getEligibleAgentsMagenta()
        {
            return ConfigurationManager.AppSettings["eligibleAgentsMagenta"].Split(';');
        }

        public string[] getInEligibleAgents()
        {
            return ConfigurationManager.AppSettings["inEligibleAgents"].Split(';');
        }

        public string[] getEligibleAgents()
        {
            return ConfigurationManager.AppSettings["EligibleAgents"].Split(';');
        }

        public bool filterMasterData() {

            try
            {
                if (masterPolicies.Count == 0)
                {
                    throw new Exception("There was no valid policies to select from PolicyMaster table");
                }
                List<Exception> insertedExceptions = new List<Exception>();
               
                foreach (var d in masterPolicies)
                {
                    try
                    {
                        var e = filterMasterPolicyRow(d);
                        filteredMasterDataList.Add(e);
                    }
                    catch (Exception ex)
                    {
                        insertedExceptions.Add(ex);
                    }

                }
                if (insertedExceptions.Count == 0)
                {
                    Console.WriteLine("All policies from PolicyMaster have been filtered");
                    return true;
                }
                else
                {
                    Console.WriteLine("There was a problem with inserting policies to local original policies table");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", "There were " + masterPolicies.Count + " policies.\n" + insertedExceptions.Count + " were not inserted.\nPlease investigate them manually" + listOfNotInsertedRows);
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
        }

        public MasterPolicy filterMasterPolicyRow(MasterPolicy row) {
            try {

                //CHECK WHETHER ROW HAS IS_FILTERED == false
                if (row.IsFiltered == true) return row;

                //INELIGIBLE IS_CANCELLED = 1
                if (row.IsCancelled == true) {
                    row.IsEligible = false;
                    row.LastStatus = 1;
                    row.NotEligibleDueToStatus = 1;
                   row.NotEligibleDueToMessage = "The policy has been cancelled";
                    row.LastStatusMessage = "The policy has been cancelled";
                    if (isEmailValid(row.EmailAddress))
                    {
                        row.HasValidEmail = true;
                    }
                    if (updateLastStatusAndStatusMessage(row))
                    {
                        row.IsFiltered = true;
                        return row;
                    }
                    else {
                        row.LastStatus = null;
                        row.LastStatusMessage = null;
                        return row;
                    }
                }
                //ELIGIBLE IS_CANCELLED = 0 -> proceed further check

                //CHECK INELIGIBLE PROMOTIONAL CODE: STAFF25, DFF, DSD
                string[] staffCodes = getStaffPromotionalCodes();
                if (staffCodes.Contains(row.PromotionalCode)) {
                    //customer is not eligible to get the gift card because he used staff discount code
                    row.IsEligible = false;
                    row.LastStatus = 2;
                    row.NotEligibleDueToStatus = 2;
                    row.NotEligibleDueToMessage = "You are a Debenhams Staff Member";
                    row.LastStatusMessage = "You are a Debenhams Staff Member";
                    if (isEmailValid(row.EmailAddress))
                    {
                        row.HasValidEmail = true;
                    }
                    if (updateLastStatusAndStatusMessage(row))
                    {
                        row.IsFiltered = true;
                        return row;
                    }
                    else
                    {
                        row.LastStatus = null;
                        row.LastStatusMessage = null;
                        return row;
                    }
                }

                //INELIGIBLE MAGENTA: 3128
                string[] magentaInEligibleAgents = getInEligibleAgentsMagenta();
                if (magentaInEligibleAgents.Contains(row.AgentId)) {
                    //customer is not eligible to get gift card because magenta AgentId is not eligble
                    row.IsEligible = false;
                    row.LastStatus = 3;
                    row.LastStatusMessage = "Your Policy was booked through "+row.AgentName+" (ineligible agent)";
                    row.NotEligibleDueToStatus = 3;
                    row.NotEligibleDueToMessage = "Your Policy was booked through " + row.AgentName + " (ineligible agent)";
                    if (isEmailValid(row.EmailAddress))
                    {
                        row.HasValidEmail = true;
                    }
                    if (updateLastStatusAndStatusMessage(row))
                    {
                        row.IsFiltered = true;
                        return row;
                    }
                    else
                    {
                        row.LastStatus = null;
                        row.LastStatusMessage = null;
                        return row;
                    }
                }

                //INELIGIBLE ORIGINAL:     <add key="inEligibleAgents" value="965;961;964;962;963;880;885;882;883;884;"/>
                string[] inEligibleAgents = getInEligibleAgents();
                if (inEligibleAgents.Contains(row.AgentId))
                {
                    //customer is not eligible to get gift card because original AgentId is not eligble
                    row.IsEligible = false;
                    row.LastStatus = 3;
                    row.LastStatusMessage = "Your Policy was booked through " + row.AgentName + " (ineligible agent)";
                    row.NotEligibleDueToStatus = 3;
                    row.NotEligibleDueToMessage = "Your Policy was booked through " + row.AgentName + " (ineligible agent)";
                    if (isEmailValid(row.EmailAddress))
                    {
                        row.HasValidEmail = true;
                    }
                    if (updateLastStatusAndStatusMessage(row))
                    {
                        row.IsFiltered = true;
                        return row;
                    }
                    else
                    {
                        row.LastStatus = null;
                        row.LastStatusMessage = null;
                        return row;
                    }
                }

                //ELIGIBLE MAGENTA: 3111, 3129
                //ElIGIBLE ORIGINAL:     <add key="eligibleAgents" value="889;888;970;916;887;951;956;970;889;971;888;916;966;956;951"/>
                string[] magentaEligibleAgents = getEligibleAgentsMagenta();
                string[] EligibleAgents = getEligibleAgents();

                if (magentaEligibleAgents.Contains(row.AgentId) || EligibleAgents.Contains(row.AgentId))
                {
                    //customer is not eligible to get gift card because magenta AgentId is not eligble
                    row.IsEligible = true;
                    if (isEmailValid(row.EmailAddress))
                    {
                        row.HasValidEmail = true;
                    }
                    row.IsFiltered = true;
                    if (updateLastStatusAndStatusMessage(row)) {
                        return row;
                    }
                    else
                    {
                        row.IsEligible = false;
                        row.IsFiltered = false;
                        row.HasValidEmail = false;
                        return row;
                    }
                }
                return row;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool isEmailValid(string email) {
            try
            {
                //contains @rockinsurance.com
                var addr = new System.Net.Mail.MailAddress(email);
                string emailToCheck = email.Trim();
                bool isEmail = Regex.IsMatch(emailToCheck, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (emailToCheck != null && isEmail == true /*&& !emailToCheck.Contains("rockinsurance.com")*/)
                {
                    return true;
                }
                else {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

        public bool updateLastStatusAndStatusMessage(MasterPolicy row)
        {
            try
            {
                var masterDataContext = new MasterData();
                if (masterDataContext.updateLastStatusAndStatusMessage(row))
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        public bool checkWhetherProvidedMonthBarcodesExists(string year, string month) {
            //i.e. up to 15 Nov polciies from 1st Nov will be allocated with October Barcodes and starting from 15 th Nov
            try
            {

                var masterDataContext = new MasterData();
                if (masterDataContext.checkWhetherProvidedMonthBarcodesExists(year,month) != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
            
        }

        public bool allocatesBarcodes() {
            try {
                DateTime nowDate = DateTime.Now;

                //check this month
                string year = nowDate.Year.ToString();
                string month = nowDate.ToString("MM");

                //check previous month
                var lastMonthDate = DateTime.Now.AddMonths(-1);
                var prevYear = lastMonthDate.Year.ToString();
                var prevMonth = lastMonthDate.ToString("MM");

                //use current month now
                //few examples..
                //you have eligible policy whic suppose to get the gift card on:
                // 1.11.2015  - will get the gift card from October pool
                // 15.11.2015 - will get the gift card from October pool
                // 16.11.2015 - will get the gift card from December
                //TripType 1 - ST = barcode £5
                //TripType 2 - AMT = barcode £10
                if (checkWhetherProvidedMonthBarcodesExists(year, month))
                {
                    //will get fresh barcode
                    //Check whether IS_FILTERED == true && HAS_BARCODE_BEEN_ALLOCATED == false
                    allocateBarcodesToFilteredAndEligiblePolicies(year, month);
                    //SET HAS_BARCODE_BEEN_ALLOCATED == TRUE
                    return true;
                }else if(checkWhetherProvidedMonthBarcodesExists(prevYear, prevMonth)) {
                    //will get barcode from previous month
                    allocateBarcodesToFilteredAndEligiblePolicies(prevYear, prevMonth);
                    return true;
                }
                else
                {
                    //there was a problem with finding barcodes from current and previous month - send notification
                    string message = "There was a problem with allocating barcodes to policies: \nneither current month nor previous month barcodes are available for allocation. \nTake immediate action: Check what has happened with barcodes from current month..";
                    Console.WriteLine(message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", message);
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }

        }

        public bool allocateBarcodesToFilteredAndEligiblePolicies(string year, string month) {
            try
            {
                if (filteredMasterDataList.Count == 0)
                {
                    throw new Exception("There were no filtered policies to allocate the barcodes 1");
                }
                List<Exception> insertedExceptions = new List<Exception>();
                var masterDataContext = new MasterData();
                
                foreach (var filteredPolicy in filteredMasterDataList)
                {
                    try
                    {
                        if (filteredPolicy.HasBarcodeBeenAllocated == false && filteredPolicy.LastStatus == null && filteredPolicy.LastStatusMessage == null)
                        {
                            var e = masterDataContext.allocateBarcode(year, month, filteredPolicy);
                            filteredWithAllocatedBarcodesMasterDataList.Add(e);
                        }
                        else
                        {
                            filteredWithAllocatedBarcodesMasterDataList.Add(filteredPolicy);
                        }

                    }
                    catch (Exception ex)
                    {
                        insertedExceptions.Add(ex);
                    }

                }
                if (insertedExceptions.Count == 0)
                {
                    Console.WriteLine("All eligible, not cancelled policies have been updated with barcode data");
                    return true;
                }
                else
                {
                    Console.WriteLine("There were no filtered policies to allocate the barcodes 2");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", "There were " + filteredMasterDataList.Count + " policies to be allocated with the barcodes.\n" + insertedExceptions.Count + " were not allocated.\nPlease investigate them manually" + listOfNotInsertedRows);
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
        }

        public bool updatePure360Statuses()
        {

            try
            {
                if (masterPolicies.Count == 0)
                {
                    throw new Exception("There were no policies to seek for their barcodes in pure360 statuses table");
                }
                List<Exception> insertedExceptions = new List<Exception>();
                var masterDataContext = new MasterData();
                foreach (var d in masterPolicies)
                {
                    try
                    {
                        if (d.HasBarcodeBeenAllocated == true && d.IsCancelled == false && d.IsEligible == true && d.HasValidEmail == true && d.LastStatus != null & d.LastStatusMessage != null)
                        {
                            var e = masterDataContext.updatePure360Statuses(d);
                        }

                    }
                    catch (Exception ex)
                    {
                        insertedExceptions.Add(ex);
                    }

                }
                if (insertedExceptions.Count == 0)
                {
                    Console.WriteLine("All emails' statuses in PolicyMaster table are up-to-date now");
                    return true;
                }
                else
                {
                    Console.WriteLine("There were a problem with updating emails statuses");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", "There were " + filteredMasterDataList.Count + " policies to be allocated with the barcodes.\n" + insertedExceptions.Count + " were not allocated.\nPlease investigate them manually" + listOfNotInsertedRows);
                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }




        }

        public bool allocatesActions() {
            try
             {
                if (filteredWithAllocatedBarcodesMasterDataList.Count == 0)
                {
                    throw new Exception("There were no filtered policies with allocated barcodes to proceed");
                }
                List<Exception> insertedExceptions = new List<Exception>();
                var masterDataContext = new MasterData();

                foreach (var d in filteredWithAllocatedBarcodesMasterDataList)
                {
                    try
                    {
                        masterDataContext.allocatesActions(d);

                    }
                    catch (Exception ex)
                    {
                        insertedExceptions.Add(ex);
                    }

                }
                if (insertedExceptions.Count == 0)
                {
                    Console.WriteLine("All eligible, not cancelled policies have been updated with actions");
                    return true;
                }
                else
                {
                    Console.WriteLine("There was a problem with allocating actions for eligible, not cancelled, filtered policies with the barcodes...");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", "There were " + filteredMasterDataList.Count + " policies to be allocated with the barcodes.\n" + insertedExceptions.Count + " were not allocated.\nPlease investigate them manually" + listOfNotInsertedRows);
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
        }

        public bool getAllReplacementBarcodes() {
            try
            {
                var MasterDataContext = new MasterData();
                var listDb = MasterDataContext.getAllReplacementBarcodes();
                if (listDb == null)
                {
                    return false;
                }
                BarcodeReplacementList = listDb;
                Console.WriteLine("All barcodes data from Barcode Replacement table have been selected");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
        }

        public bool checkWhetherIssueNewBarcodes() {
            try
            {
                DateTime nowDate = DateTime.Now;

                //check this month
                string year = nowDate.Year.ToString();
                string month = nowDate.ToString("MM");

                //check previous month
                var lastMonthDate = DateTime.Now.AddMonths(-1);
                var prevYear = lastMonthDate.Year.ToString();
                var prevMonth = lastMonthDate.ToString("MM");

                //use current month now
                //few examples..
                //you have eligible policy whic suppose to get the gift card on:
                // 1.11.2015  - will get the gift card from October pool
                // 15.11.2015 - will get the gift card from October pool
                // 16.11.2015 - will get the gift card from December
                //TripType 1 - ST = barcode £5
                //TripType 2 - AMT = barcode £10

              
                if (checkWhetherProvidedMonthBarcodesExists(year, month))
                {
                    //will get fresh barcode
                    //Check whether IS_FILTERED == true && HAS_BARCODE_BEEN_ALLOCATED == false
                    issueNewBarcodes(year, month);
                    //SET HAS_BARCODE_BEEN_ALLOCATED == TRUE
                    return true;
                }
                else if (checkWhetherProvidedMonthBarcodesExists(prevYear, prevMonth))
                {
                    //will get barcode from previous month
                    issueNewBarcodes(prevYear, prevMonth);
                    return true;
                }
                else
                {
                    //there was a problem with finding barcodes from current and previous month - send notification
                    string message = "There was a problem with allocating barcodes to policies: \nneither current month nor previous month barcodes are available for allocation. \nTake immediate action: Check what has happened with barcodes from current month..";
                    Console.WriteLine(message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", message);
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
        }

        public bool checkWhetherIssueNewBarcodesReplace(string barcode)
        {
            try
            {
                DateTime nowDate = DateTime.Now;

                //check this month
                string year = nowDate.Year.ToString();
                string month = nowDate.ToString("MM");

                //check previous month
                var lastMonthDate = DateTime.Now.AddMonths(-1);
                var prevYear = lastMonthDate.Year.ToString();
                var prevMonth = lastMonthDate.ToString("MM");

                //use current month now
                //few examples..
                //you have eligible policy whic suppose to get the gift card on:
                // 1.11.2015  - will get the gift card from October pool
                // 15.11.2015 - will get the gift card from October pool
                // 16.11.2015 - will get the gift card from December
                //TripType 1 - ST = barcode £5
                //TripType 2 - AMT = barcode £10


                if (checkWhetherProvidedMonthBarcodesExists(year, month))
                {
                    //will get fresh barcode
                    //Check whether IS_FILTERED == true && HAS_BARCODE_BEEN_ALLOCATED == false
                    issueNewBarcodesReplace(year, month, barcode);
                    //SET HAS_BARCODE_BEEN_ALLOCATED == TRUE
                    return true;
                }
                else if (checkWhetherProvidedMonthBarcodesExists(prevYear, prevMonth))
                {
                    //will get barcode from previous month
                    issueNewBarcodesReplace(prevYear, prevMonth, barcode);
                    return true;
                }
                else
                {
                    //there was a problem with finding barcodes from current and previous month - send notification
                    string message = "There was a problem with allocating barcodes to policies: \nneither current month nor previous month barcodes are available for allocation. \nTake immediate action: Check what has happened with barcodes from current month..";
                    Console.WriteLine(message);
                    EmailHandler emailHandler = new EmailHandler();
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", message);
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }
        }


        public bool issueNewBarcodes(string year, string month) {
            try
            {
                List<Exception> insertedExceptions = new List<Exception>();
                List<Exception> insertedExceptions2 = new List<Exception>();
                MasterData MasterDataContext = new MasterData();
                if (filteredMasterDataList.Count != 0)
                {

                    foreach (var filteredPolicy in filteredMasterDataList)
                    {
                        try
                        {
                            if (filteredPolicy.HasItBeenReplaced == false && filteredPolicy.LastStatus == 9)
                            {
                                var e = MasterDataContext.issueNewBarcodesForMasterPolicyTable(year, month, filteredPolicy);
                            }

                        }
                        catch (Exception ex)
                        {
                            insertedExceptions.Add(ex);
                        }

                    }
                }
                if (BarcodeReplacementList.Count != 0)
                {
                    foreach (var barcode in BarcodeReplacementList)
                    {
                        try
                        {
                            if (barcode.HasItBeenReplaced == false && barcode.LastStatus == 9 )
                            {
                                var e = MasterDataContext.issueNewBarcodesForBarcodeReplacementTable(year, month, barcode);
                            }

                        }
                        catch (Exception ex)
                        {
                            insertedExceptions2.Add(ex);
                        }

                    }
                }
             

               
               
                if (insertedExceptions.Count == 0 && insertedExceptions2.Count == 0)
                {
                    Console.WriteLine("All policies with expired barcodes, checked by operations the haven'tbeen used, have been allocated with new barcodes");
                    
                }
                else
                {
                    Console.WriteLine("There were a problem with issuing the new barcodes");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", "There were " + filteredMasterDataList.Count + " policies to be allocated with the barcodes.\n" + insertedExceptions.Count + " were not allocated.\nPlease investigate them manually" + listOfNotInsertedRows);
                    return false;
                }
               

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }

        }

        public bool issueNewBarcodesReplace(string year, string month, string barcodeNo)
        {
            try
            {
                List<Exception> insertedExceptions = new List<Exception>();
                List<Exception> insertedExceptions2 = new List<Exception>();
                MasterData MasterDataContext = new MasterData();
                if (filteredMasterDataList.Count != 0)
                {

                    foreach (var filteredPolicy in filteredMasterDataList)
                    {
                        try
                        {
                            if (filteredPolicy.HasItBeenReplaced == false && filteredPolicy.LastStatus == 9 && filteredPolicy.BarcodeNumber == barcodeNo)
                            {
                                var e = MasterDataContext.issueNewBarcodesForMasterPolicyTable(year, month, filteredPolicy);
                            }

                        }
                        catch (Exception ex)
                        {
                            insertedExceptions.Add(ex);
                        }

                    }
                }
                if (BarcodeReplacementList.Count != 0)
                {
                    foreach (var barcode in BarcodeReplacementList)
                    {
                        try
                        {
                            if (barcode.HasItBeenReplaced == false && barcode.LastStatus == 9)
                            {
                                var e = MasterDataContext.issueNewBarcodesForBarcodeReplacementTable(year, month, barcode);
                            }

                        }
                        catch (Exception ex)
                        {
                            insertedExceptions2.Add(ex);
                        }

                    }
                }




                if (insertedExceptions.Count == 0 && insertedExceptions2.Count == 0)
                {
                    Console.WriteLine("All policies with expired barcodes, checked by operations the haven'tbeen used, have been allocated with new barcodes");

                }
                else
                {
                    Console.WriteLine("There were a problem with issuing the new barcodes");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", "There were " + filteredMasterDataList.Count + " policies to be allocated with the barcodes.\n" + insertedExceptions.Count + " were not allocated.\nPlease investigate them manually" + listOfNotInsertedRows);
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule Exception", ex.Message);
                return false;
            }

        }


        public bool getAllDataForRefusalsEmails() {
            try
            {
                MasterData masterContext = new MasterData();
                var listDb = masterContext.getAllDataForRefusalsEmails();
                if (listDb == null)
                {
                    return false;
                }
                RefusalDataList = listDb;
                Console.WriteLine("All the refusals data has been selected");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);

                return false;
            }
        }

        public bool insertDataToRefusal()
        {

            try
            {
                List<Exception> myExceptionList = new List<Exception>();
                foreach (var refusalRow in RefusalDataList)
                {
                    try
                    {
                        MasterData myMasterData = new MasterData();
                        myMasterData.insertDataToRefusal(refusalRow);
                    }
                    catch (Exception ex)
                    {
                        myExceptionList.Add(ex);
                    }
                }
                if (myExceptionList.Count == 0)
                {
                   Console.WriteLine("Data has been inserted to RefusalEmailCampaignTable");
                    return true;
                }

                if (myExceptionList.Count > 0)
                {
                    int allRows = RefusalDataList.Count;
                    int exceptionsRows = myExceptionList.Count;
                    string message = "MasterFeed RefusalEmail data: There was a problem with inserting refusal email data to RefusalEmailCampaign.\nThere was " + allRows + " rows. \n" + exceptionsRows + " were not inserted.";
                    Console.WriteLine(message);
                    string additionalMessage = "\nThere were following exceptions: \n";
                    foreach (var d in myExceptionList)
                    {
                        additionalMessage += "\n Exception Message " + d.Message + ", Inner Exception: " + d.InnerException + ", Exception Source: " + d.Source + ", Exception StackTrace: " + d.StackTrace + "\n";
                    }
                    throw new Exception(message + "\n" + additionalMessage);
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was a problem with inserting the refusal data to RefusalEmailCampaign table");
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("MasterModule exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
                return false;

            }

          
        }
    }
}

