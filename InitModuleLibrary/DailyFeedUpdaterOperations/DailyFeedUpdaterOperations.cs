﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Configuration;
using InitModuleLibrary.Model;
using InitModuleLibrary.OriginalPolicyOperations;
using InitModuleLibrary.Data;
using InitModuleLibrary.EmailNotificationHandler;


namespace InitModuleLibrary.DailyFeedUpdaterOperations
{
    public class DailyFeedUpdaterOperations : iDailyFeedUpdaterOperations
    {

        private List<MagentaPolicyFeed> _magentaPolicyFeedPolicies = null;
        private List<OriginalPolicyFeed> _originalPolicyFeedPolicies = null;
        
        public List<MagentaPolicyFeed> MagentaPolicyFeedPolicies
        {
            get
            {
                return _magentaPolicyFeedPolicies;
            }

            set
            {
                _magentaPolicyFeedPolicies = value;
            }
        }

        public List<OriginalPolicyFeed> OriginalPolicyFeedPolicies
        {
            get
            {
                return _originalPolicyFeedPolicies;
            }

            set
            {
                _originalPolicyFeedPolicies = value;
            }
        }
        
        public bool getPoliciesFromMagentaPolicyFeedTable()
        {
            try
            {
                var orgDataContext = new DailyFeedUpdaterData();
                var listDb = orgDataContext.getMagentaPolicyFeedPolicies();
                if (listDb == null)
                {
                   return false;
                }
                MagentaPolicyFeedPolicies = listDb;
                Console.WriteLine("Policies from MagentPolicyaFeed Table has been copied");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", ex.Message);
                return false;
            }

        }

        public bool getPoliciesFromOriginalPolicyFeedTable()
        {
            try
            {
                var orgDataContext = new DailyFeedUpdaterData();
                var listDb = orgDataContext.getOriginalPolicyFeedPolicies();
                if (listDb == null)
                {
                    return false;
                }
                OriginalPolicyFeedPolicies = listDb;
                Console.WriteLine("Policies from MagentaFeed Table has been copied");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Feed Exception", ex.Message);
                return false;
            }
        }

        public bool insertMagentaPoliciesIntoPolicyMasterTable()
        {
            try
            {
                if (MagentaPolicyFeedPolicies.Count == 0)
                {
                    throw new Exception("There were no valid policies to copy from MagentaPolicyFeed table from that day");
                }
                var daily = new DailyFeedUpdaterData();
                List<Exception> insertedExceptions = new List<Exception>();
                foreach (var d in MagentaPolicyFeedPolicies)
                {
                    try
                    {
                        daily.insertMagentaPoliciesIntoPolicyMasterTable(d);
                    }
                    catch (Exception ex)
                    {
                        insertedExceptions.Add(ex);
                    }

                }
                if (insertedExceptions.Count == 0)
                {
                    Console.WriteLine("All policies from MagentaPolicyFeed table has been inserted to PolicyMaster table");
                    return true;
                }
                else
                {
                    Console.WriteLine("There was a problem with inserting Magenta policies into PolicyMaster table");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", "There were " + MagentaPolicyFeedPolicies.Count + " magenta policies.\n" + insertedExceptions.Count + " were not inserted.\nPlease investigate them manually" + listOfNotInsertedRows);
                }
                return false;


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", ex.Message);
                return false;
            }
        }

        public bool insertOriginalPoliciesIntoPolicyMasterTable()
        {
            try
            {
                if (OriginalPolicyFeedPolicies.Count == 0)
                {
                    throw new Exception("There were no valid policies to copy from OriginalPolicyFeed table from that day");
                }
                var daily = new DailyFeedUpdaterData();
                List<Exception> insertedExceptions = new List<Exception>();
                foreach (var d in OriginalPolicyFeedPolicies)
                {
                    try
                    {
                        daily.insertOriginalPoliciesIntoPolicyMasterTable(d);
                    }
                    catch (Exception ex)
                    {
                        insertedExceptions.Add(ex);
                    }

                }
                if (insertedExceptions.Count == 0)
                {
                    Console.WriteLine("All policies from OriginalPolicyFeed table has been inserted to PolicyMaster table");
                    return true;
                }
                else
                {
                    Console.WriteLine("There was a problem with inserting Original policies into PolicyMaster table");
                    EmailHandler emailHandler = new EmailHandler();
                    string listOfNotInsertedRows = "\nThere were following exceptions:\n";
                    foreach (var e in insertedExceptions)
                    {
                        listOfNotInsertedRows += "\n" + e.Message + "\n";
                    }
                    emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater Exception", "There were " + OriginalPolicyFeedPolicies.Count + " original policies.\n" + insertedExceptions.Count + " were not inserted.\nPlease investigate them manually" + listOfNotInsertedRows);
                }
                return false;


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("DailyFeedUpdater", ex.Message);
                return false;
            }
        }

    }
     
}




