﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.IO;
using InitModuleLibrary.Model;
using InitModuleLibrary.Data;

namespace InitModuleLibrary.DailyFeedUpdaterOperations
{
    public interface iDailyFeedUpdaterOperations
    {
        bool getPoliciesFromOriginalPolicyFeedTable();
        bool getPoliciesFromMagentaPolicyFeedTable();
        bool insertMagentaPoliciesIntoPolicyMasterTable();
        bool insertOriginalPoliciesIntoPolicyMasterTable();
        List<OriginalPolicyFeed> OriginalPolicyFeedPolicies { get; set; }
        List<MagentaPolicyFeed> MagentaPolicyFeedPolicies { get; set; }

    }
}
