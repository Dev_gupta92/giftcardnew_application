﻿using System;
using System.Collections.Generic;

namespace InitModuleLibrary.EmailNotificationHandler
{
    interface iEmailHandler
    {
        string emailMessage { get; set; }
        List<string> emailsToBeNotified { get; set; }

        bool sendEmailWithExceptionOrError(string emailTitle, string message);
        bool sendEmailWithWSuccess(string emailTitle, string message);
        bool sendEmail(string emailTitle, string message);
        void setListOfEmailsNeedToBeNotified();


    }
}
