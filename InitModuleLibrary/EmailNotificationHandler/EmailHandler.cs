﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Net.Mail;

namespace InitModuleLibrary.EmailNotificationHandler
{
    public class EmailHandler : iEmailHandler
    {
        private List<string> _emailsToBeNotified = null;
        private string _emailMessage = String.Empty;
        public EmailHandler() {
            setListOfEmailsNeedToBeNotified();
        }

        public List<string> emailsToBeNotified
        {
            get
            {
                return _emailsToBeNotified;
            }
            set {
                _emailsToBeNotified = value;
            }
        }
        public string emailMessage
        {
            get
            {
                return _emailMessage;
            }

            set
            {
                _emailMessage = value;
            }
        }

        #region sendEmailWithExceptionOrError
        public bool sendEmailWithExceptionOrError(string emailTitle, string message)
        {

            if (emailsToBeNotified.Count < 1)
            {
                return false;
            }
            if (sendEmail(emailTitle, message))
            {
                Console.WriteLine("Emails have been sent...");
                return true;
            }
            else
            {

                Console.WriteLine("Emails have not been sent...");
                return false;
            }
        }
        #endregion

        #region sendEmailWithWSuccess
        public bool sendEmailWithWSuccess(string emailTitle, string message)
        {
            if (emailsToBeNotified.Count < 1) {
                return false;
            }
            if (sendEmail(emailTitle, message))
            {
                Console.WriteLine("Emails have been sent...");
                return true;
            }
            else {
              
                Console.WriteLine("Emails have not been sent...");
                return false;
            }
            
        }
        #endregion

        #region setListOfEmailsNeedToBeNotified
        public void setListOfEmailsNeedToBeNotified()
        {
            emailsToBeNotified = ConfigurationManager.AppSettings["emails"].Split(';').ToList();
        }
        #endregion

        #region sendEmail
        public bool sendEmail(string status, string message)
        {
            try
            {
               MailMessage mailMessage = new MailMessage("mariusz.rajczakowski@rock.insurance.com", emailsToBeNotified[0], status, message);
                //MailAddress emailCC = new MailAddress("chandra.chandrabalan@rockinsurance.com");
                //mailMessage.CC.Add(emailCC);
                SmtpClient smtpClient = new SmtpClient();
                if (emailsToBeNotified.Count > 1)
                {
                    for (int i = 1; i < emailsToBeNotified.Count; i++)
                    {
                        MailAddress emailCC = new MailAddress(emailsToBeNotified[i]);
                        mailMessage.CC.Add(emailCC);
                    }
                }
                smtpClient.Send(mailMessage);

            }catch(Exception ex)
            {
                Console.WriteLine("Emails Error...");
            }
            return true;
        }
        #endregion
    }
}
