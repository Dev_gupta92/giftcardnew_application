﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Renci.SshNet;

namespace InitModuleLibrary.Validations
{
    interface iFileNameFormatValidation
    {

        ////validate if file has proper name pattern
        bool debenhamsFilesExists(IEnumerable<Renci.SshNet.Sftp.SftpFile> myFiles, SftpClient client);
        bool magentaFileExists(IEnumerable<Renci.SshNet.Sftp.SftpFile> myFiles, SftpClient client,DateTime processDate, bool checkPrevDay);
        void MagentaFileStringify();
        MemoryStream File1Mem { get; set; }
        MemoryStream File2Mem { get; set; }
        MemoryStream MagentaFileMem { get; set; }
        string [] MagentaFileRowsStringified { get; set; }
        string FileName1 { get; set; }
        string FileName2 { get; set; }
        string FileFormat { get; set; }
        string[] File1RowsStringified { get; set; }
        string[] File2RowsStringified { get; set; }
        ////validate if file is .csv format
        //bool fileFormatIsValid(System.IO.Stream s);


    }
}
