﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Configuration;

namespace InitModuleLibrary.Validations
{
    public class Validation : iFileNameFormatValidation, iFileHeadingValidation, iFileRecordValidation
    {

        private MemoryStream _file1Mem = null;
        private MemoryStream _file2Mem = null;
        private MemoryStream _file3Mem = null;
        private MemoryStream _file4Mem = null;
        private MemoryStream _magentaFileMem = null;
        private string _fileName1 = ConfigurationManager.AppSettings["barcodeFileName1"];
        private string _fileName2 = ConfigurationManager.AppSettings["barcodeFileName2"];
        private string _fileName3 = ConfigurationManager.AppSettings["barcodeFileName3"];
        private string _fileName4 = ConfigurationManager.AppSettings["barcodeFileName4"];

        private string _unprocessedfileName1 = ConfigurationManager.AppSettings["unprocessedfile1"];
        private string _unprocessedfileName2 = ConfigurationManager.AppSettings["unprocessedfile2"];
        private string _unprocessedfileName3 = ConfigurationManager.AppSettings["unprocessedfile3"];
        private string _unprocessedfileName4 = ConfigurationManager.AppSettings["unprocessedfile4"];



        private bool _dboffer = Convert.ToBoolean(ConfigurationManager.AppSettings["DBOffer"]);
        private string _magentaFile = ConfigurationManager.AppSettings["magentaFile"];
        private string _fileFormat = ConfigurationManager.AppSettings["fileFormat"];
        private string _magentaDir = ConfigurationManager.AppSettings["MagentaDir"];
        private string _debDirectory = ConfigurationManager.AppSettings["DebenhamsDir"];
        public string DebebhamsDir
        {
            get
            {
                return _debDirectory;
            }

            set
            {
                _debDirectory = value;
            }
        }
        public string MagentaDir
        {
            get
            {
                return _magentaDir;
            }

            set
            {
                _magentaDir = value;
            }
        }
        private string[] _file1RowsStringified = null;
        private string[] _file2RowsStringified = null;
        private string[] _file3RowsStringified = null;
        private string[] _file4RowsStringified = null;
        private string[] _magentaFileRowsStringified;

        public string yearMonthDir = string.Empty;

        public string[] File1RowsStringified
        {
            get
            {
                return _file1RowsStringified;
            }

            set
            {
                _file1RowsStringified = value;
            }
        }

        public string[] File2RowsStringified
        {
            get
            {
                return _file2RowsStringified;
            }

            set
            {
                _file2RowsStringified = value;
            }
        }

        public string[] File3RowsStringified
        {
            get
            {
                return _file3RowsStringified;
            }

            set
            {
                _file3RowsStringified = value;
            }
        }

        public string[] File4RowsStringified
        {
            get
            {
                return _file4RowsStringified;
            }

            set
            {
                _file4RowsStringified = value;
            }
        }

        public string[] MagentaFileRowsStringified
        {
            get
            {
                return _magentaFileRowsStringified;
            }

            set
            {
                _magentaFileRowsStringified = value;
            }
        }


        public MemoryStream File1Mem
        {
            get
            {
                return _file1Mem;
            }

            set
            {
                _file1Mem = value;
            }
        }
        public MemoryStream File2Mem
        {
            get
            {
                return _file2Mem;
            }

            set
            {
                _file2Mem = value;
            }
        }
        public MemoryStream File3Mem
        {
            get
            {
                return _file3Mem;
            }

            set
            {
                _file3Mem = value;
            }
        }

        public MemoryStream File4Mem
        {
            get
            {
                return _file4Mem;
            }

            set
            {
                _file4Mem = value;
            }
        }
        public MemoryStream MagentaFileMem
        {
            get
            {
                return _magentaFileMem;
            }

            set
            {
                _magentaFileMem = value;
            }
        }

        public string MagentaFile
        {
            get
            {
                return _magentaFile;
            }

            set
            {
                _magentaFile = value;
            }
        }

        public string FileName1
        {
            get
            {
                return _fileName1;
            }

            set
            {
                _fileName1 = value;
            }
        }

        public string FileName2
        {
            get
            {
                return _fileName2;
            }

            set
            {
                _fileName2 = value;
            }
        }
        public string FileName3
        {
            get
            {
                return _fileName3;
            }

            set
            {
                _fileName3 = value;
            }
        }

        public string FileName4
        {
            get
            {
                return _fileName4;
            }

            set
            {
                _fileName4 = value;
            }
        }

        public string unprocessedfileName1
        {
            get
            {
                return _unprocessedfileName1;
            }

            set
            {
                _unprocessedfileName1 = value;
            }
        }

        public string unprocessedfileName2
        {
            get
            {
                return _unprocessedfileName2;
            }

            set
            {
                _unprocessedfileName2 = value;
            }
        }
        public string unprocessedfileName3
        {
            get
            {
                return _unprocessedfileName3;
            }

            set
            {
                _unprocessedfileName3 = value;
            }
        }

        public string unprocessedfileName4
        {
            get
            {
                return _unprocessedfileName4;
            }

            set
            {
                _unprocessedfileName4 = value;
            }
        }




        public bool DBOffer
        {
            get
            {
                return _dboffer;
            }
            set
            {
                _dboffer = value;
            }
        }

        public string FileFormat
        {
            get
            {
                return _fileFormat;
            }

            set
            {
                _fileFormat = value;
            }
        }

        #region isHeadingValid
        //FILE HEADINGS VALIDATION

        public bool isHeadingValid()
        {
            //how many columns should be
            string[] columnNames = ConfigurationManager.AppSettings["columnNames"].Split(';');
            int howManyColumnsShouldBe = columnNames.Length;

            //how many columns there are in file 1
            string file1Heading = File1RowsStringified[0];
            string[] file1HeadingColumnArray = file1Heading.Split(',');
            int file1HeadingColumnNumber = file1HeadingColumnArray.Length;

            //how many columns there are in file 2
            string file2Heading = File2RowsStringified[0];
            string[] file2HeadingColumnArray = file2Heading.Split(',');
            int file2HeadingColumnNumber = file2HeadingColumnArray.Length;

            //how many columns there are in file 3
            string file3Heading = string.Empty;
            string[] file3HeadingColumnArray = null;
            int file3HeadingColumnNumber = 0;

            if (File3RowsStringified != null)
            {
                file3Heading = File3RowsStringified[0];
                file3HeadingColumnArray = file3Heading.Split(',');
                file3HeadingColumnNumber = file3HeadingColumnArray.Length;
            }

            //how many columns there are in file 4
            string file4Heading = string.Empty;
            string[] file4HeadingColumnArray = null;
            int file4HeadingColumnNumber = 0;

            if (File4RowsStringified != null)
            {
                file4Heading = File4RowsStringified[0];
                file4HeadingColumnArray = file4Heading.Split(',');
                file4HeadingColumnNumber = file4HeadingColumnArray.Length;
            }

            #region old Code
            //if (file1HeadingColumnNumber != howManyColumnsShouldBe || file2HeadingColumnNumber != howManyColumnsShouldBe)
            //{
            //    throw new Exception("Files headings number is invalid");
            //}

            //for (int i = 0; i < howManyColumnsShouldBe; i++)
            //{
            //    if (columnNames[i].Equals(file1HeadingColumnArray[i]) == false || columnNames[i].Equals(file2HeadingColumnArray[i]) == false)
            //    {

            //        throw new Exception("Files headings names are not valid");
            //    }
            //}
            #endregion old Code

            if (file1HeadingColumnNumber != howManyColumnsShouldBe || file2HeadingColumnNumber != howManyColumnsShouldBe || (file3HeadingColumnNumber != howManyColumnsShouldBe && DBOffer == true) || (file4HeadingColumnNumber != howManyColumnsShouldBe && DBOffer == true))
            {
                throw new Exception("Files headings number is invalid");
            }

            for (int i = 0; i < howManyColumnsShouldBe; i++)
            {
                if (columnNames[i].Equals(file1HeadingColumnArray[i]) == false || columnNames[i].Equals(file2HeadingColumnArray[i]) == false)
                {

                    throw new Exception("Files headings names are not valid");
                }
                if (DBOffer && File3RowsStringified != null)
                {
                    if (columnNames[i].Equals(file3HeadingColumnArray[i]) == false)
                    {
                        throw new Exception("File 3 headings names are not valid");
                    }

                }
                if (DBOffer && File4RowsStringified != null)
                {
                    if (columnNames[i].Equals(file4HeadingColumnArray[i]) == false)
                    {
                        throw new Exception("File 4 headings names are not valid");
                    }
                }
            }
            return true;
        }


        public bool isHeadingValidunProcessedFile()
        {
            string[] columnNames = ConfigurationManager.AppSettings["unprocessedcolumnNames"].Split(';');
            int howManyColumnsShouldBe = columnNames.Length;

            //how many columns there are in file 1
            string file1Heading = File1RowsStringified[0];
            string[] file1HeadingColumnArray = file1Heading.Split(',');
            int file1HeadingColumnNumber = file1HeadingColumnArray.Length;

            //how many columns there are in file 2
            string file2Heading = File2RowsStringified[0];
            string[] file2HeadingColumnArray = file2Heading.Split(',');
            int file2HeadingColumnNumber = file2HeadingColumnArray.Length;

            if (file1HeadingColumnNumber != howManyColumnsShouldBe || file2HeadingColumnNumber != howManyColumnsShouldBe)
            {
                throw new Exception("Files headings number is invalid");
            }

            for (int i = 0; i < howManyColumnsShouldBe; i++)
            {
                var col = columnNames[i];
                var col1 = file1HeadingColumnArray[i];
                var col2 = file2HeadingColumnArray[i];


                if (columnNames[i].Equals(file1HeadingColumnArray[i]) == false || columnNames[i].Equals(file2HeadingColumnArray[i]) == false)
                {

                    throw new Exception("Files headings names are not valid");
                }
            }


            return true;
        }

        #endregion



        #region isMagentaHeadingValid
        //FILE HEADINGS VALIDATION

        public bool isMagentaHeadingValid()
        {
            //how many columns should be
            string[] columnNames = ConfigurationManager.AppSettings["columnNames"].Split(';');
            int howManyColumnsShouldBe = columnNames.Length;

            //how many columns there are in Magenta file
            string magentaHeading = MagentaFileRowsStringified[0];



            string[] magentaHeadingColumnArray = magentaHeading.Split(',');
            int magentaHeadingColumnNumber = magentaHeadingColumnArray.Length;
            if (magentaHeadingColumnNumber != howManyColumnsShouldBe)
            {
                throw new Exception("Magenta File headings number is invalid");
            }

            for (int i = 0; i < howManyColumnsShouldBe; i++)
            {
                if (magentaHeadingColumnArray[i].Contains(columnNames[i]) == false)
                {
                    throw new Exception("Magenta File headings names are not valid");
                }
                //if ((columnNames[i]).Equals(magentaHeadingColumnArray[i]) == false )
                //{
                //    throw new Exception("Magenta File headings names are not valid");
                //}
            }
            return true;
        }

        #endregion

        #region filesStringify
        public void filesStringify()
        {
            //stringify file 1
            string File1Stringified = Encoding.ASCII.GetString(File1Mem.ToArray());
            string[] dd1 = new[] { "\r\n" };
            File1RowsStringified = File1Stringified.Split(dd1, StringSplitOptions.RemoveEmptyEntries);
            //stringify file 2
            string File2Stringified = Encoding.ASCII.GetString(File2Mem.ToArray());
            string[] dd2 = new[] { "\r\n" };
            File2RowsStringified = File2Stringified.Split(dd2, StringSplitOptions.RemoveEmptyEntries);


        }

        #endregion

        #region MagentaFileStringify
        public void MagentaFileStringify()
        {
            //stringify magenta file
            string MagentaFileStringified = Encoding.ASCII.GetString(MagentaFileMem.ToArray());
            string[] dd1 = new[] { "\r\n" };

            string[] rows = MagentaFileStringified.Split(dd1, StringSplitOptions.RemoveEmptyEntries);
            string pattern = @"""\s*,\s*""";
            var t = new List<string>();
            foreach (string d in rows)
            {
                t.Add(System.Text.RegularExpressions.Regex.Replace(d, pattern, ",").Replace("\"", string.Empty));
            }

            MagentaFileRowsStringified = t.ToArray();




            //          string[] tokens = System.Text.RegularExpressions.Regex.Split(
            //MagentaFileRowsStringified[0].Substring(1, MagentaFileRowsStringified[0].Length - 2), pattern);




            //MagentaFileRowsStringified=
        }

        #endregion

        #region magentaFileExists
        public bool magentaFileExists(IEnumerable<SftpFile> myFiles, SftpClient client, DateTime processDate, bool checkPrevDay)
        {
            string year = processDate.Year.ToString();
            string month = processDate.ToString("MM");
            string day;
            if (checkPrevDay)
            {

                day = processDate.AddDays(-1).ToString("dd");
            }
            else
            {
                day = processDate.ToString("dd");
            }

            //get file names for current month
            //file name pattern: Magenta_2015_10_15.csv
            //string fileFull = MagentaFile + year + "_" + month + "_" + day + "." + FileFormat;
            string fileFull = "merged20180723.csv";// "." + FileFormat;
            string pathToFile = MagentaDir + fileFull;
            bool fileExist = client.Exists(pathToFile);

            if (fileExist == false)
            {
                throw new Exception("MagentaFeed: Cannot process the file. \nThere is no files from previous day (" + day + "/" + month + "/" + year + ") \nin MagentaFeed directory \n" + pathToFile);
            }

            var memo = new MemoryStream();
            client.DownloadFile(pathToFile, memo);
            MagentaFileMem = memo;

            client.Disconnect();
            return true;
        }
        #endregion

        #region debenhamsFilesExists
        public bool debenhamsFilesExists(IEnumerable<SftpFile> myFiles, SftpClient client)
        {

            //get the current date
            DateTime nowDate = DateTime.Now;
            //get year
            string year = nowDate.Year.ToString();
            string month = nowDate.Month.ToString();

            //get file names for current month
            //file 1 name pattern: five_pounds_barcodes_201510.csv
            string file1 = FileName1 + "_" + year + month + "." + FileFormat;

            //file 2 name pattern: ten_pounds_barcodes_201510.csv
            string file2 = FileName2 + "_" + year + month + "." + FileFormat;

            //file 3 name pattern: fifteen_pounds_barcodes_201612.csv
            string file3 = FileName3 + "_" + year + month + "." + FileFormat;

            //file 4 name pattern: twenty_pounds_barcodes_201612.csv
            string file4 = FileName4 + "_" + year + month + "." + FileFormat;

            bool file1Exist = client.Exists(DebebhamsDir + "/" + year + "/" + file1);
            bool file2Exist = client.Exists(DebebhamsDir + "/" + year + "/" + file2);
            bool file3Exist = client.Exists(DebebhamsDir + "/" + year + "/" + file3);
            bool file4Exist = client.Exists(DebebhamsDir + "/" + year + "/" + file4);


            if (file1Exist == false && file2Exist == false && file3Exist == false && file4Exist == false)
            {
                throw new Exception("There are no files in provided directory");
            }

            if (file1Exist == false)
            {
                throw new Exception("File '" + file1 + "' does not exist in provided directory");
            }
            if (file2Exist == false)
            {
                throw new Exception("File '" + file2 + "' does not exist in provided directory");
            }

            //Checks whether Debenhams Offers is applied for current month or not
            if (file3Exist == false)
            {
                if (DBOffer == true)
                {
                    throw new Exception("File '" + file3 + "' does not exist in provided directory");
                }

            }

            //Checks whether Debenhams Offers is applied for current month or not
            if (file4Exist == false)
            {
                if (DBOffer == true)
                {
                    throw new Exception("File '" + file4 + "' does not exist in provided directory");
                }

            }

            var m1 = new MemoryStream();
            client.DownloadFile(DebebhamsDir + "/" + year + "/" + file1, m1);
            File1Mem = m1;
            m1 = new MemoryStream();
            client.DownloadFile(DebebhamsDir + "/" + year + "/" + file2, m1);
            File2Mem = m1;
            //Works only when both file exist and Debenhams offer can be applied for the current month
            if (file3Exist)
            {
                m1 = new MemoryStream();
                client.DownloadFile(DebebhamsDir + "/" + year + "/" + file3, m1);
                File3Mem = m1;
            }

            if (file4Exist)
            {
                m1 = new MemoryStream();
                client.DownloadFile(DebebhamsDir + "/" + year + "/" + file4, m1);
                File4Mem = m1;
            }
            client.Disconnect();
            return true;
        }


        #endregion

        #region unprocesseddebenhamsFilesExists
        public bool unprocesseddebenhamsFilesExists(IEnumerable<SftpFile> myFiles, SftpClient client)
        {
            //get the current date
            DateTime nowDate = DateTime.Now;
            //get year
            string year = nowDate.Year.ToString();
            string month = nowDate.Month.ToString();

            //get file names for current month
            //file 1 name pattern: five_pounds_barcodes_201510.csv
            string file1 = unprocessedfileName1 + "." + FileFormat;

            //file 2 name pattern: ten_pounds_barcodes_201510.csv
            string file2 = unprocessedfileName2 + "." + FileFormat;

            //file 3 name pattern: fifteen_pounds_barcodes_201612.csv
            string file3 = unprocessedfileName3 + "." + FileFormat;

            //file 4 name pattern: twenty_pounds_barcodes_201612.csv
            string file4 = unprocessedfileName4 + "." + FileFormat;

            bool file1Exist = client.Exists(DebebhamsDir +  "/" + file1);
            bool file2Exist = client.Exists(DebebhamsDir + "/" + file2);
            bool file3Exist = client.Exists(DebebhamsDir  + "/" + file3);
            bool file4Exist = client.Exists(DebebhamsDir +  "/" + file4);
            if (file1Exist == false && file2Exist == false )
            {
                throw new Exception("There are no files in provided directory");
            }

            if (file1Exist == false)
            {
                throw new Exception("File '" + file1 + "' does not exist in provided directory");
            }
            if (file2Exist == false)
            {
                throw new Exception("File '" + file2 + "' does not exist in provided directory");
            }

            var m1 = new MemoryStream();
            client.DownloadFile(DebebhamsDir + "/" + file1, m1);
            File1Mem = m1;
            m1 = new MemoryStream();
            client.DownloadFile(DebebhamsDir + "/"  + file2, m1);
            File2Mem = m1;

            client.Disconnect();
            return true;
        }
     
        #endregion

    }
}
