﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace InitModuleLibrary.Validations
{
    interface iFileHeadingValidation
    {
        //validate whether file has the exact number of columns
        bool isHeadingValid();
        bool isMagentaHeadingValid();


    }
}
