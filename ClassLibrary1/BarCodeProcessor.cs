﻿using InitModuleLibrary.DebenhamsOperations;
using InitModuleLibrary.EmailNotificationHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCApp
{
    public class BarCodeProcessor
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("BarcodeProcessor module");
                Console.WriteLine("Start");
                DebenhamsOperations operation = new DebenhamsOperations();
                Console.WriteLine("Trying to connect and grab the unprocessed files from SFTP");
                operation.UnprocessedFile();

                //Console.WriteLine("Trying to connect and grab the files from SFTP");
                //operation.connectAndGrabTheFile();
                Console.WriteLine("Trying to process the files");
                operation.processFiles();
                Console.WriteLine("Trying to insert data to database");
                EmailHandler emailHandler = new EmailHandler();
                operation.insertFilesToDatabase();
                Console.WriteLine("Trying to create directory for images");
                if (!operation.createFolderStructureForCurrentMonth()) { Console.WriteLine("End"); /*Console.ReadLine();*/ return; }
                Console.WriteLine("Trying to set flag HasBeenInsertedTrue");
                if (!operation.setHasBeenInsertedToTrue()) { Console.WriteLine("End"); /*Console.ReadLine(); */return; }
                emailHandler.sendEmailWithWSuccess("BarcodeProcessor: success", "Congratulations! \nBarcodes for this month has been sucessfully inserted!");
                Console.WriteLine("Congratulations! \nBarcodes for this month has been sucessfully inserted!");
                Console.WriteLine("End");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                EmailHandler emailHandler = new EmailHandler();
                emailHandler.sendEmailWithExceptionOrError("BarcodeProcessor module exception", "Exception Message: " + ex.Message + "\nException Code: " + ex.InnerException + "\nException Source: " + ex.Source + "\nException StackTrace: " + ex.StackTrace + "\nException TargetSite: " + ex.TargetSite);
            }

        }
    }
}
